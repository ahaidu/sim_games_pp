sim_games_pp
============

Catkin package for post processing data from the  [sim_games](https://bitbucket.org/ahaidu/sim_games) package. The gazebo logs will be replayed using a server plugin and tf / raw / events data will be logged to MongoDB / OWL files.

# Prerequisites #

 * [Gazebo](http://gazebosim.org) - for re-running the episodes
 * [ROS](http://www.ros.org/install/) - logging tf data, communication with other ros packages
 * [MongoDB](https://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/) - storing the tf/raw data
 * [26compat](https://github.com/mongodb/mongo-cxx-driver/tree/26compat) - the mongodb cxx driver
 * [libconfig](http://www.hyperrealm.com/libconfig/) - reading the configuration files on how to save the data
 * [semrec](https://github.com/code-iai/semrec) - OWL exporter


# Configuration files #

Configuration files are stored in the ```/config``` folder and are read using ```libconfig```. This helps on configuring on which data to store during post processing.

### Config explanations ###

```
# Post processing related
pp:
{
	ignore = "oculus"
	raw = true			  # process or not raw data
	tf = true			    # process or not tf data
	events = true			# process or not event data
};
```
 * `ignore` = models to ignore (do not log their data)
 * `raw` = log raw data or not
 * `tf` = log tf data or not
 * `events` = log events or not (contact, grasping, etc. events)


```
#MongoDB related variables
mongo:
{
	db_name = "ACAT1-db"
 	coll_name = "acat1_coll_"
};
```
 * `db_name` = mongo database name on where to store the data
 * `coll_name` = collection name, to which a suffix argument (`--suffix #`) will be appended


```
# Simulation related variables
sim:
{
	world_name = "acat1_world"	  # world name to be loaded
};
```
 * `world_name` = gazebo world to be played (the name of the world saved in the [sim_games](https://bitbucket.org/ahaidu/sim_games) package).


```
# LogTF related variables
tf:
{
  	publish = false			      # publish tf on a topic
  	write_all_tf = false
  	dist_thresh = 0.01		      # meter
  	angular_thresh = 0.05		      # rad
  	duration_thresh = 100	   	      # ms
};
```
 * `publish` = publish or not the tf data during the post processing
 * `write_all_transf` = write all tf data without taking into account thresholds
 * `dist_thresh` = log tf where the distance (meter) threshold has been crossed
 * `angular_thresh` = log tf where the rotation (radians) threshold has been crossed
 * `duration_thresh` = log tf where the duration (ms) threshold has been crossed


```
# LogRaw related variables
raw:
{
  	write_all_raw = false
  	dist_thresh = 0.001			  # meter
  	angular_thresh = 0.01		          # rad
};
```
 * `write_all_raw` = write all raw data without taking into account thresholds
 * `dist_thresh` = log raw where the distance (meter) threshold has been crossed
 * `angular_thresh` = log raw where the rotation (radians) threshold has been crossed


```
# LogEvents related variables
events:
{
  	# location where to log the events (owl, mongo, all)
  	log_location = "owl"

  	# duration (sec) between particles leaving the containers
  	# in order to be counted as different events
  	transf_ev_dur_thresh = 0.5

  	# threshold for concatenating shortly disconnected events (sec)
  	ev_disc_thresh = 0.5

  	# collisions used for supporting event detection
  	surface_colls = [
  			 "kitchen_floor_event_collision",
  			 "table_top_event_collision"
  	];

  	# collisions used for grasp detection (contact 'sensors')
  	grasp_colls = [
  		       "r_gripper_r_finger_tip_event_collision",
  		       "r_gripper_l_finger_tip_event_collision"
  	];

  	# translation event, particle leaving container
        # (contact collision, liquid model name)
  	transl_colls = (
  			("florence_flask_event_collision", "ChemLiq"),
  			("gas_jar_event_collision", "ChemLiq")
        );

  	# manipulation tool surface
        # (collision , manipulated models names)
  	tool_colls = (
  		      ("spoon_event_collision", "Champignon", "Cheese", "Olive", "Bacon")
  	);
};
```
 * `log_location` = where to store the events, owl files, mongodb, or both (all)
 * `transf_ev_dur_thresh` = threshold for counting as different events when particles are translated from a container (time (sec) between the particles leaving)
 * `ev_disc_thresh` = threshold in order to concatenate events which shortly disconnect due to physics engine instability
 * `surface_colls` = collisions (usually of [`<collide_without_contact>`](http://sdformat.org/spec?ver=1.6&elem=collision#contact_collide_without_contact) type) used for saving surface contact events (e.g. Object on Table)
 * `grasp_colls` = collisions (usually of [`<collide_without_contact>`](http://sdformat.org/spec?ver=1.6&elem=collision#contact_collide_without_contact) type) used for grasp checking, if all the given collisions are in contact with the same object, a grasp event is triggered
 * `transl_colls` = collisions (usually of [`<collide_without_contact>`](http://sdformat.org/spec?ver=1.6&elem=collision#contact_collide_without_contact) type) used for particle translations, usually located at the openings of the containers to check when particles are leaving
 * `tool_colls` = similar to `transl_colls`, however the particles are always in contact, and it checks when contact disappears


# Usage #

 * start `semrec` and a `roscore` in a terminal (if events should be logged):
~~~
$ roscore
$ rosrun semrec semrec
~~~

 * make sure the mongodb service is running (if raw data should be saved)

 * start replaying the gazebo episode using the post-processing plugin:
 
ACAT:
~~~
gazebo -u --verbose -s libPostProcess.so -p logs/acat/exp07/7.1/gzserver/state.log --suffix 1 --conf config/acat/exp07.cfg --offset 0

~~~

PIZZA:
~~~
gazebo -u --verbose -s libPostProcess.so -p logs/pizza_2016/pizza_4/gzserver/state.log --suffix 4 --conf config/pizza_config.cfg --offset 0
~~~
* `-p` = path to the gazebo log file
* `--suffix` = suffix to be appended at the end of the collection name
* `--conf` = path to the config file
* `--offset` = time offset of every episode sometimes needed for tf queries

CHEM:
~~~
gazebo -u --verbose -s libPostProcess.so -p logs/chem/c1/gzserver/state.log --suffix 1 --conf config/chem_config.cfg  --offset 0
~~~