/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013-16, Andrei Haidu, Institute for Artificial Intelligence,
 *  Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#ifndef LOG_EVENTS_HH
#define LOG_EVENTS_HH

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
// #include <beliefstate_client/BeliefstateClient.h>
// #include <beliefstate_client/Context.h>
#include <libconfig.h++>
#include "mongo/client/dbclient.h"
#include "GzEvent.hh"


#include </home/haidu/sandbox/catkin_ws/src/semrec_client/include/beliefstate_client/BeliefstateClient.h>
#include </home/haidu/sandbox/catkin_ws/src/semrec_client/include/beliefstate_client/Context.h>

/// \brief Post Processing classes namespace
namespace sg_pp
{
/// \brief class LogEvents
class LogEvents
{
    /// \brief Constructor
    public: LogEvents(const gazebo::physics::WorldPtr _world,
            const std::string _db_name,
            const std::string _coll_name,
            const int _suffix,
            const std::string _config_file,
            const double _time_offset);

    /// \brief Destructor
    public: virtual ~LogEvents();

    /// \brief Initialise events
    public: void InitEvents();

    /// \brief Check for semantic events
    public: void CheckEvents();

    /// \brief Finalize events
    public: void FiniEvents();

    /// \brief Load config file
    private: void ReadConfigFile();
    
    /// \brief Check custom embedded events
    private: void CheckCustomEmbeddedEv();
    
    /// \brief Check current Grasp
    private: void CheckGraspEvent(
            const double _timestamp_ms,
            const unsigned int _grasp_contacts_nr,
            const std::set<std::string> &_grasped_models);

    /// \brief Check current left Grasp
    private: void LCheckGraspEvent(
            const double _timestamp_ms,
            const unsigned int _grasp_contacts_nr,
            const std::set<std::string> &_grasped_models);
    
    /// \brief Check current event collisions
    private: void CheckSurfaceEvents(
            const double _timestamp_ms,
            const std::set<std::pair<std::string, std::string>> &_curr_surface_models_in_contact);

    /// \brief Check translation event
    private: void CheckTranslEvent(
            const double _timestamp_ms,
            const bool _transf_detect_flag);
    
    /// \brief Check translation event with goal location
    private: void CheckTranslEventWGoalLocation(
            const double _timestamp_ms,
            const bool _transf_detect_flag,
            const std::string _transf_goal_model_name);
    
    /// \brief Check translation event with from location
    private: void CheckTranslEventWFromLocation(
            const double _timestamp_ms,
            const bool _transf_detect_flag,
            const std::string _transf_from_model_name);
    
    /// \brief Check model translation event
    private: void CheckModelTranslEvent(
            const double _timestamp_ms,
            const bool _transf_detect_flag);
    
    /// \brief Check tool event
    private: void CheckToolEvent(
            const double _timestamp_ms,
            const std::string _contact_with_tool_model,
            const std::string _tool_name,
            const std::string _tool_collision_name);

    /// \brief Check tool part event
    private: void CheckToolPartEvent(
            const double _timestamp_ms,
            const std::string _contact_with_tool_part_model,
            const std::string _tool_part_name,
            const std::string _tool_part_collision_name);
    
    /// \brief Merge short disconnections in the events timeline
    private: void MergeEventDisconnections();
    
    /// \brief Remove short grasp jitters
    private: void RemoveGraspJitters();

    /// \brief End still active events
    private: void EndActiveEvents();

    /// \brief Write beliefstate (owl) contexts
    private: void WriteContexts();

    /// \brief Write timelines to file
    private: void WriteTimelines();

    /// \brief Write ADT properties to the context
    private: void WriteADTActions(PpEvent* _pp_event, beliefstate_client::Context* _ctx);
    
    /// \brief Return name as CamelCase
    private: std::string ToCamelCase(const std::string _name);
    
    /// \brief Return name as camelCase without the first letter
    private: std::string ToCamelCaseWithoutFirst(const std::string _name);
    
    /// \brief Config file path
    private: std::string configFile;
    
    /// \brief Time offset for logging TF
    private: double timeOffset;
    
    /// \brief Gazebo world
    private: const gazebo::physics::WorldPtr world;

    /// \brief pointer of ContactManager, for getting contacts from physics engine
    private: gazebo::physics::ContactManager *contactManagerPtr;

    /// \brief Beliefstate client
    private: beliefstate_client::BeliefstateClient* beliefStateClient;

    /// \brief Database name
    private: const std::string dbName;

    /// \brief Db collection name
    private: const std::string collName;

    /// \brief Log location of the events
    private: std::string logLocation;
    
    /// \brief Flag if custom emvedded events are available in the log files
    private: bool customEmbeddedEv;
    
    // TODO for adding time offset to the simulation times
    private: int suffixTime;

    /// \brief Event disconnection threshold limit
    private: double eventDiscThresh;

    /// \brief Transfer event duration thresh
    private: double transfEvDurThresh;
    
    /// \brief Grasp jitter duration
    private: double graspJitterDur;
    
    /// \brief Previous state of models being in contact with surfaces
    /// Set with <surface model name, model in contact with>
    private: std::set<std::pair<std::string, std::string>>
        prevSurfaceModelsInContact;

    // TODO change container, vector? to have access to the next value?
    /// \brief Map of event names to GzEvents
    private: std::map<std::string, std::list<sg_pp::PpEvent*>> nameToEvents_M;

    // TODO change, needed so every object appears once for knowrob hashing
    /// \brief Map of all the objects name from the simulation to beliefstate objects
    private: std::map<std::string, beliefstate_client::Object*> nameToBsObject_M;

    // TODO remove this
    /// \brief Model names to GzEventObj map
    private: std::map<std::string, sg_pp::PpEventObj*> nameToEventObj_M;

    /// \brief Surface collision names to check in the world
    private: std::set<std::string> surfaceCollNames;

    /// \brief Surface collisions to check in the world
    private: std::set<gazebo::physics::Collision*> surfaceColls;
    
    /// \brief Model translation collision names
    private: std::set<std::string> modelTranslCollNames;
    
    /// \brief Model translation collisions
    private: std::set<gazebo::physics::Collision*> modelTranslColls;
    
    /// \brief Transfered model
    private: gazebo::physics::ModelPtr transferedModel;

    // TODO add init to avoid checking for NULl
    /// \brief Grasp GzEvent
    private: sg_pp::PpEvent* graspGzEvent;

    /// \brief Grasp collision names to check for grasp contacts
    private: std::set<std::string> graspCollNames;
    
    /// \brief Grasp collisions to check for grasp contacts
    private: std::set<gazebo::physics::Collision*> graspColls;
        
    /// \brief name of the grasped model
    private: std::string prevGraspedModel;
    
    // TODO add init to avoid checking for NULl
    /// \brief Left Grasp GzEvent
    private: sg_pp::PpEvent* l_graspGzEvent;
    
    /// \brief Left grasp collision names to check for grasp contacts
    private: std::set<std::string> l_graspCollNames;

    /// \brief name of the grasped model
    private: std::string l_prevGraspedModel;        
    
    /// \brief Left Grasp collisions to check for grasp contacts
    private: std::set<gazebo::physics::Collision*> l_graspColls;
    
    /// \brief Container collision names to particle model names
    private: std::map<std::string, std::set<std::string>> contCollToPNames;
    
    /// \brief Container collision names to particle model names with goal location
    private: std::map<std::string, std::set<std::string>> contCollToPNamesWGoalLocation;
    
    /// \brief Container collision names to particle model names with from location
    private: std::map<std::string, std::set<std::string>> contCollToPNamesWFromLocation;

    /// \brief Container collisions to particle models
    private: std::map<gazebo::physics::Collision*,
        std::set< gazebo::physics::ModelPtr>> contCollToPModels;
        
    /// \brief Container collisions to particle models with goal location
    private: std::map<gazebo::physics::Collision*,
        std::set< gazebo::physics::ModelPtr>> contCollToPModelsWGoalLocation;
        
    /// \brief Container collisions to particle models with from location
    private: std::map<gazebo::physics::Collision*,
        std::set< gazebo::physics::ModelPtr>> contCollToPModelsWFromLocation;

    /// \brief Transfered particles of the given model
    private: std::map<gazebo::physics::ModelPtr,
        std::map<gazebo::physics::Collision*, bool>> transferedParticles;

    /// \brief Transfered particles of the given model with goal location
    private: std::map<gazebo::physics::ModelPtr,
        std::map<gazebo::physics::Collision*, bool>> transferedParticlesWGoalLocation;

    /// \brief Transfered particles of the given model with from location
    private: std::map<gazebo::physics::ModelPtr,
        std::map<gazebo::physics::Collision*, bool>> transferedParticlesWFromLocation;        
        
    /// \brief Number of transfered particles of the given model
    private: std::map<gazebo::physics::ModelPtr, unsigned int>
        transferedPartCount;

    /// \brief Number of transfered particles of the given model with goal location
    private: std::map<gazebo::physics::ModelPtr, unsigned int>
        transferedPartCountWGoalLocation;
        
    /// \brief Number of transfered particles of the given model with from location
    private: std::map<gazebo::physics::ModelPtr, unsigned int>
        transferedPartCountWFromLocation;  
        
    /// \brief Timestamp of latest particle leaving the container
    private: double transfTs;

    /// \brief Timestamp of latest particle leaving the container with goal location
    private: double transfTsWGoalLocation;

    /// \brief Timestamp of latest particle leaving the container with from location
    private: double transfTsWFromLocation;
    
    /// \brief Pool of the transfered particles in the current transfer event
    private: std::vector<gazebo::physics::Collision*> transfParticlePool;

    /// \brief Pool of the transfered particles in the current transfer event with goal location
    private: std::vector<gazebo::physics::Collision*> transfParticlePoolWGoalLocation;

    /// \brief Pool of the transfered particles in the current transfer event with from location
    private: std::vector<gazebo::physics::Collision*> transfParticlePoolWFromLocation;    
    
    // TODO works for one transfer per time (mixed particles in a bowl would not work)
    /// \brief Transf event
    private: sg_pp::PpEvent* transfEvent;
    
    /// \brief Transf event with goal location
    private: sg_pp::PpEvent* transfEventWGoalLocation;
    
    /// \brief Transf event with from location
    private: sg_pp::PpEvent* transfEventWFromLocation;
    
    /// \brief Model transfer event
    private: sg_pp::PpEvent* modelTransfEvent;

    /// \brief Tool collision names to particle model names
    private: std::map<std::string, std::set<std::string>> toolCollToPNames;
    
    /// \brief Tool collision part names to colliding model names
    private: std::map<std::string, std::set<std::string>> toolCollPartToModelNames;

    /// \brief Tool collisions to particle models
    private: std::map<gazebo::physics::Collision*,
        std::set< gazebo::physics::ModelPtr>> toolCollToPModels;
        
    /// \brief Tool collisions to particle models
    private: std::map<gazebo::physics::Collision*,
        std::set< gazebo::physics::ModelPtr>> toolCollPartToModels;
        
    /// \brief Name of the prev contact with the tool
    private: std::string prevContactWithToolModel;
    
    /// \brief Name of the prev contact with the tool part
    private: std::string prevContactWithToolPartModel;

    // TODO generalize for multiple events
    /// \brief Tool event
    private: sg_pp::PpEvent* toolGzEvent;    

    // TODO generalize for multiple events
    /// \brief Tool part collision event
    private: sg_pp::PpEvent* toolPartGzEvent;    
    
    /// \brief ACAT ADT Flag if acat adts should be logged
    private: bool acatADT;
    
    /// \brief ACAT ADT PRAC ADT instruction
    private: std::set<std::tuple<std::string, std::string>> pracAdtObj;
    
};
}
#endif
