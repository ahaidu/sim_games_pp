/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013-16, Andrei Haidu, Institute for Artificial Intelligence,
 *  Universität Bremen.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Institute for Artificial Intelligence,
 *     Universität Bremen, nor the names of its contributors may be
 *     used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "LogEvents.hh"
#include "tinyxml2.h"
#include "gazebo/util/LogPlay.hh"

using namespace sg_pp;
using namespace gazebo;
using namespace mongo;

//////////////////////////////////////////////////
LogEvents::LogEvents(const gazebo::physics::WorldPtr _world,
        const std::string _db_name,
        const std::string _coll_name,
        const int _suffix,
        const std::string _config_file,
        const double _time_offset) :
        world(_world),
        dbName(_db_name),
        collName(_coll_name),
        suffixTime(_suffix),
        configFile(_config_file),
        timeOffset(_time_offset)
{
    // get the world models
    this->contactManagerPtr = this->world->GetPhysicsEngine()->GetContactManager();

    // get values from the config file
    LogEvents::ReadConfigFile();

    // if events are exported as owl  init ros
    if(this->logLocation == "owl" || this->logLocation == "all")
    {
        // intialize ROS
        int argc = 0;
        char** argv = NULL;
        ros::init(argc, argv, "owl_events");
    }
}

//////////////////////////////////////////////////
LogEvents::~LogEvents()
{
}

//////////////////////////////////////////////////
void LogEvents::ReadConfigFile()
{
    // create the config
    libconfig::Config cfg;

    // read config file
    try
    {
        cfg.readFile(this->configFile.c_str());
    }
    catch(const libconfig::FileIOException &fioex)
    {
        std::cerr << "I/O error while reading file." << std::endl;
    }
    catch(const libconfig::ParseException &pex)
    {
        std::cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine()
                              << " - " << pex.getError() << std::endl;
    }

    // get the variables from the config file
    // log location owl/mongo/all ..
    try
    {
        this->logLocation = cfg.lookup("events.log_location").c_str();
        std::cout << "*LogEvents* - log_location: " << this->logLocation << std::endl;
    }
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        this->logLocation = "all";
        std::cerr << "*LogEvents* !!! - [events.log_location] not found." <<
        " - log_location: " << this->logLocation << std::endl;
    }
    
    // duration between particles leaving container to count as the same event
    try
    {
        this->transfEvDurThresh = cfg.lookup("events.transf_ev_dur_thresh");
        std::cout << "*LogEvents* - transf_ev_dur_thresh: " << this->transfEvDurThresh << std::endl;
    }
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        this->transfEvDurThresh = 0.5;
        std::cerr << "*LogEvents* !!! - [events.transf_ev_dur_thresh] not found." <<
        " - transf_ev_dur_thresh: " << this->transfEvDurThresh << std::endl;
    }
    
    // threshold for concatenating shortly disconected events (sec)
    try
    {
        this->eventDiscThresh = cfg.lookup("events.ev_disc_thresh");
        std::cout << "*LogEvents* - ev_disc_thresh: " << this->eventDiscThresh << std::endl;
    }
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        this->eventDiscThresh = 0.1;
        std::cerr << "*LogEvents* !!! - [events.ev_disc_thresh] not found." <<
        " - ev_disc_thresh: " << this->eventDiscThresh << std::endl;
    }
    
    // threshold for removing grasp jitter
    try
    {
        this->graspJitterDur = cfg.lookup("events.grasp_jitter_dur");
        std::cout << "*LogEvents* - grasp_jitter_dur: " << this->graspJitterDur << std::endl;
    }
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        this->graspJitterDur = 0.0;
        std::cerr << "*LogEvents* !!! - [events.grasp_jitter_dur] not found." <<
        " - grasp_jitter_dur: " << this->graspJitterDur << std::endl;
    }
    
    // get the collisions used for grasping detection
    try
    {    
        // get the grasp collisions from config file
        libconfig::Setting& grasp_coll_cf = cfg.lookup("events.grasp_colls");
        std::cout << "*LogEvents* - grasp_colls: " << std::endl;
        
        // insert surface coll names to set
        for(int i = 0; i < grasp_coll_cf.getLength(); ++i)
        {
            this->graspCollNames.insert(grasp_coll_cf[i]);
            std::cout << "\t" << grasp_coll_cf[i].c_str() << std::endl;
        }
    }
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        std::cerr << "*LogEvents* !!! - [events.grasp_colls] not found." <<
        " - grasp_colls: none "  << std::endl;
    }
        
    // get the collisions used for left grasping detection
    try
    {    
        // get the left grasp collisions from config file
        libconfig::Setting& l_grasp_coll_cf = cfg.lookup("events.l_grasp_colls");
        std::cout << "*LogEvents* - l_grasp_colls: " << std::endl;
        
        // insert surface coll names to set
        for(int i = 0; i < l_grasp_coll_cf.getLength(); ++i)
        {
            this->l_graspCollNames.insert(l_grasp_coll_cf[i]);
            std::cout << "\t" << l_grasp_coll_cf[i].c_str() << std::endl;
        }
    }
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        std::cerr << "*LogEvents* !!! - [events.l_grasp_colls] not found." <<
        " - l_grasp_colls: none "  << std::endl;
    }
    
    // get the collision used for surface contact event
    try
    {
        // get the surface collisions from config file
        libconfig::Setting& surface_coll_cf = cfg.lookup("events.surface_colls");
        std::cout << "*LogEvents* - surface_colls: " << std::endl;

        // insert surface coll names to set
        for(int i = 0; i < surface_coll_cf.getLength(); ++i)
        {
            this->surfaceCollNames.insert(surface_coll_cf[i]);
            std::cout << "\t" << surface_coll_cf[i].c_str() << std::endl;
        }
    }
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        std::cerr << "*LogEvents* !!! - [events.surface_colls] not found." <<
        " - surface_colls: none "  << std::endl;
    }

    // translation events, particle leaving containers
    try
    {        
        // get the translation pairs <container top sensor, [particle types] >
        libconfig::Setting& transl_colls_cf = cfg.lookup("events.transl_colls");
        std::cout << "*LogEvents* - transl_colls: " << std::endl;

        // map container (transl) coll to particle names
        for(int i = 0; i < transl_colls_cf.getLength(); ++i)
        {
            // key - name of the container coll
            const std::string coll_key = transl_colls_cf[i][0].c_str();
            std::cout << "\t" << coll_key << " -> ";

            // values - names of the particle types to check for collision
            std::set<std::string> particle_names;

            // loop through the rest, and add particle names
            // notice that we skip the first value (j = 0) which is the coll name (key)
            for(int j = 1; j < transl_colls_cf[i].getLength(); ++j)
            {
                particle_names.insert(transl_colls_cf[i][j].c_str());

                std::cout << transl_colls_cf[i][j].c_str() << "; ";
            }
            std::cout << std::endl;

            // add <key - value> to the map
            this->contCollToPNames.insert({coll_key, particle_names});
        }
    }
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        std::cerr << "*LogEvents* !!! - [events.transl_colls] not found." <<
        " - transl_colls: none "  << std::endl;
    }
    
    // translation events, particle leaving containers, with goal location
    try
    {        
        // get the translation pairs <container top sensor, [particle types] >
        libconfig::Setting& transl_colls_gl_cf = cfg.lookup("events.transl_colls_goal_location");
        std::cout << "*LogEvents* - transl_colls_goal_location: " << std::endl;

        // map container (transl) coll to particle names
        for(int i = 0; i < transl_colls_gl_cf.getLength(); ++i)
        {
            // key - name of the container coll
            const std::string coll_key = transl_colls_gl_cf[i][0].c_str();
            std::cout << "\t" << coll_key << " -> ";

            // values - names of the particle types to check for collision
            std::set<std::string> particle_names;

            // loop through the rest, and add particle names
            // notice that we skip the first value (j = 0) which is the coll name (key)
            for(int j = 1; j < transl_colls_gl_cf[i].getLength(); ++j)
            {
                particle_names.insert(transl_colls_gl_cf[i][j].c_str());

                std::cout << transl_colls_gl_cf[i][j].c_str() << "; ";
            }
            std::cout << std::endl;

            // add <key - value> to the map
            this->contCollToPNamesWGoalLocation.insert({coll_key, particle_names});
        }
    }
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        std::cerr << "*LogEvents* !!! - [events.transl_colls_goal_location] not found." <<
        " - transl_colls_goal_location: none "  << std::endl;
    }
    
    // translation events, particle leaving containers, with from location
    try
    {        
        // get the translation pairs <container top sensor, [particle types] >
        libconfig::Setting& transl_colls_fl_cf = cfg.lookup("events.transl_colls_from_location");
        std::cout << "*LogEvents* - transl_colls_from_location: " << std::endl;

        // map container (transl) coll to particle names
        for(int i = 0; i < transl_colls_fl_cf.getLength(); ++i)
        {
            // key - name of the container coll
            const std::string coll_key = transl_colls_fl_cf[i][0].c_str();
            std::cout << "\t" << coll_key << " -> ";

            // values - names of the particle types to check for collision
            std::set<std::string> particle_names;

            // loop through the rest, and add particle names
            // notice that we skip the first value (j = 0) which is the coll name (key)
            for(int j = 1; j < transl_colls_fl_cf[i].getLength(); ++j)
            {
                particle_names.insert(transl_colls_fl_cf[i][j].c_str());

                std::cout << transl_colls_fl_cf[i][j].c_str() << "; ";
            }
            std::cout << std::endl;

            // add <key - value> to the map
            this->contCollToPNamesWFromLocation.insert({coll_key, particle_names});
        }
    }
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        std::cerr << "*LogEvents* !!! - [events.transl_colls_from_location] not found." <<
        " - transl_colls_from_location: none "  << std::endl;
    }
        
    // manipulation tool surface
    try
    {
        // get the tool collision pairs <tool sensor, [particle types] >
        libconfig::Setting& tool_colls_cf = cfg.lookup("events.tool_colls");
        std::cout << "*LogEvents* - tool_colls: " << std::endl;

        // map tool coll to particle names
        for(int i = 0; i < tool_colls_cf.getLength(); ++i)
        {
            // key - name of the container coll
            const std::string coll_key = tool_colls_cf[i][0].c_str();
            std::cout << "\t" << coll_key << " -> ";

            // values - names of the particle types to check for collision
            std::set<std::string> particle_names;

            // loop through the rest, and add particle names
            // notice that we skip the first value (j = 0) which is the coll name (key)
            for(int j = 1; j < tool_colls_cf[i].getLength(); ++j)
            {
                particle_names.insert(tool_colls_cf[i][j].c_str());

                std::cout << tool_colls_cf[i][j].c_str() << "; ";
            }
            std::cout << std::endl;

            // add <key - value> to the map
            this->toolCollToPNames.insert({coll_key, particle_names});
        }
    }
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        std::cerr << "*LogEvents* !!! - [events.tool_colls] not found." <<
        " - tool_colls: none "  << std::endl;
    }
    
    // tool part collision
    try
    {        
        // get the tool part collision pairs <tool part, [colliding model types] >
        libconfig::Setting& tool_part_colls_cf = cfg.lookup("events.tool_part_colls");
        std::cout << "*LogEvents* - tool_part_colls: " << std::endl;

        // map container (transl) coll to particle names
        for(int i = 0; i < tool_part_colls_cf.getLength(); ++i)
        {
            // key - name of the container coll
            const std::string coll_key = tool_part_colls_cf[i][0].c_str();
            std::cout << "\t" << coll_key << " -> ";

            // values - names of the particle types to check for collision
            std::set<std::string> coll_models;

            // loop through the rest, and add particle names
            // notice that we skip the first value (j = 0) which is the coll name (key)
            for(int j = 1; j < tool_part_colls_cf[i].getLength(); ++j)
            {
                coll_models.insert(tool_part_colls_cf[i][j].c_str());

                std::cout << tool_part_colls_cf[i][j].c_str() << "; ";
            }
            std::cout << std::endl;

            // add <key - value> to the map
            this->toolCollPartToModelNames.insert({coll_key, coll_models});
        }
    }
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        std::cerr << "*LogEvents* !!! - [events.tool_part_colls] not found." <<
        " - tool_part_colls: none "  << std::endl;
    }
    
    // model translation event, model leaving / entering area
    try
    {
        // get model transl colls
        libconfig::Setting& model_transl_colls_cf = cfg.lookup("events.model_transl_colls");
        std::cout << "*LogEvents* - model_transl_colls: " << std::endl;

        // insert surface coll names to set
        for(int i = 0; i < model_transl_colls_cf.getLength(); ++i)
        {
            this->modelTranslCollNames.insert(model_transl_colls_cf[i]);
            std::cout << "\t" << model_transl_colls_cf[i].c_str() << std::endl;
        }
    }     
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        std::cerr << "*LogEvents* !!! - [events.model_transl_colls] not found." <<
        " - model_transl_colls: none "  << std::endl;
    }
    
    // custom embedded events in the log file
    try
    {
        this->customEmbeddedEv = cfg.lookup("events.custom_embedded_ev");
        std::cout << "*LogEvents* - custom_embedded_ev: " << this->customEmbeddedEv << std::endl;
    }
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        this->customEmbeddedEv = false;
        std::cerr << "*LogEvents* !!! - [events.custom_embedded_ev] not found." <<
        " - custom_embedded_ev: " << this->customEmbeddedEv << std::endl;
    }
    
    // ACAT ADT
    try
    {
        this->acatADT = cfg.lookup("acat_adt.write_adt");
        std::cout << "*LogEvents-ADT* - write_adt: " << this->acatADT << std::endl;
    }
    catch(const libconfig::SettingNotFoundException &snfex)
    {
        this->acatADT = false;
        std::cerr << "*LogEvents-ADT* !!! - [acat_adt.write_adt] not found." <<
        " - write_adt: " << this->acatADT << std::endl;
    }
}

//////////////////////////////////////////////////
void LogEvents::InitEvents()
{
    std::cout << "*LogEvents* - Sim start: " << this->world->GetSimTime().Double() << std::endl;

    // init ts for transfer event
    this->transfTs = this->world->GetSimTime().Double();
    this->transfTsWGoalLocation = this->world->GetSimTime().Double();
    this->transfTsWFromLocation = this->world->GetSimTime().Double();
    
    // TODO add events init
    this->transfEvent = NULL;
    this->transfEventWGoalLocation = NULL;
    this->transfEventWFromLocation = NULL;
    
    this->modelTransfEvent = NULL;

    this->graspGzEvent = NULL;
    
    this->l_graspGzEvent = NULL;

    this->toolGzEvent = NULL;
    
    this->toolPartGzEvent = NULL;

    // open the main GzEvent
    this->nameToEvents_M["Episode-Duration"].push_back( //TODO generalise episode name
            new PpEvent("Episode-Duration","&knowrob_sim;", "Episode", this->world->GetSimTime().Double()));
    
    // Look up the no contact collisions (sensors)
    // interate through all the models to see which have event collisions
    for(const auto m_iter : this->world->GetModels())
    {
        // TODO look into this, why not just create the object on spot?
        // THIS needed to ensore unique hashing of the object names when creating contexts
        // map model name to the GzEventObj object
        this->nameToEventObj_M[m_iter->GetName()] = new PpEventObj(m_iter->GetName());

        // TODO make ignore list?
        // NEEDED as well to esablish uniqueness of the context object, see if the other is still needed
        // map model name to the beliefstate object
        this->nameToBsObject_M[m_iter->GetName()] =
                new beliefstate_client::Object("&knowrob;", m_iter->GetName());

        // loop through the links
        for (const auto l_iter : m_iter->GetLinks())
        {
            // loop through all the collision
            for (const auto c_iter : l_iter->GetCollisions())
            {
                // if the collision is without physical contact then add it to the map
                if (c_iter->GetSurface()->collideWithoutContact)
                {
                    // check if grasp coll type
                    if (this->graspCollNames.find(c_iter->GetName()) != this->graspCollNames.end())
                    {
                        this->graspColls.insert(c_iter.get());
                    }
                    
                    // check if grasp coll type
                    if (this->l_graspCollNames.find(c_iter->GetName()) != this->l_graspCollNames.end())
                    {
                        this->l_graspColls.insert(c_iter.get());
                    }

                    // check if surface collision type
                    if (this->surfaceCollNames.find(c_iter->GetName()) != this->surfaceCollNames.end())
                    {
                        this->surfaceColls.insert(c_iter.get());
                    }
                    
                    // check if model transl collision type
                    if (this->modelTranslCollNames.find(c_iter->GetName()) != this->modelTranslCollNames.end())
                    {
                        this->modelTranslColls.insert(c_iter.get());
                    }

                    // check if the container collision (transl) type
                    // initialize the particles as 'untransfered'
                    const auto cont_res = this->contCollToPNames.find(c_iter->GetName());

                    if (cont_res != this->contCollToPNames.end())
                    {
                        // particle models
                        std::set< gazebo::physics::ModelPtr> particle_models;

                        // loop through particle names to find the models and add them to the vector
                        for (const auto p_it : cont_res->second)
                        {
                            // get the particle model
                            physics::ModelPtr particle_model = this->world->GetModel(p_it);

                            // insert model to the set
                            particle_models.insert(particle_model);

                            // init the models particles (collisions) as untransfered
                            // map of link to the transfered flag
                            std::map<physics::Collision*, bool> particles_coll_map;

                            // loop through all links and get the first collision
                            for (const auto l_it : particle_model->GetLinks())
                            {
                                physics::Collision* first_coll = l_it->GetCollisions()[0].get();
                                particles_coll_map[first_coll] = false;
                            }

                            // map the particles model to the untransfered links
                            // set map< model - map< link - bool > > to FALSE
                            this->transferedParticles[this->world->GetModel(p_it)] = particles_coll_map;
                        }

                        // map container collision to all particle model vector
                        this->contCollToPModels[c_iter.get()] = particle_models;
                    }
                    
                    // check if the container collision (transl) type with goal location
                    // initialize the particles as 'untransfered'
                    const auto cont_res_gl = this->contCollToPNamesWGoalLocation.find(c_iter->GetName());

                    if (cont_res_gl != this->contCollToPNamesWGoalLocation.end())
                    {
                        // particle models
                        std::set< gazebo::physics::ModelPtr> particle_models;

                        // loop through particle names to find the models and add them to the vector
                        for (const auto p_it : cont_res_gl->second)
                        {
                            // get the particle model
                            physics::ModelPtr particle_model = this->world->GetModel(p_it);

                            // insert model to the set
                            particle_models.insert(particle_model);

                            // init the models particles (collisions) as untransfered
                            // map of link to the transfered flag
                            std::map<physics::Collision*, bool> particles_coll_map;

                            // loop through all links and get the first collision
                            for (const auto l_it : particle_model->GetLinks())
                            {
                                physics::Collision* first_coll = l_it->GetCollisions()[0].get();
                                particles_coll_map[first_coll] = false;
                            }

                            // map the particles model to the untransfered links
                            // set map< model - map< link - bool > > to FALSE
                            this->transferedParticlesWGoalLocation[this->world->GetModel(p_it)] = particles_coll_map;
                        }

                        // map container collision to all particle model vector
                        this->contCollToPModelsWGoalLocation[c_iter.get()] = particle_models;
                    }
                    
                    // check if the container collision (transl) type with from location
                    // initialize the particles as 'untransfered'
                    const auto cont_res_fl = this->contCollToPNamesWFromLocation.find(c_iter->GetName());

                    if (cont_res_fl != this->contCollToPNamesWFromLocation.end())
                    {
                        // particle models
                        std::set< gazebo::physics::ModelPtr> particle_models;

                        // loop through particle names to find the models and add them to the vector
                        for (const auto p_it : cont_res_fl->second)
                        {
                            // get the particle model
                            physics::ModelPtr particle_model = this->world->GetModel(p_it);

                            // insert model to the set
                            particle_models.insert(particle_model);

                            // init the models particles (collisions) as untransfered
                            // map of link to the transfered flag
                            std::map<physics::Collision*, bool> particles_coll_map;

                            // loop through all links and get the first collision
                            for (const auto l_it : particle_model->GetLinks())
                            {
                                physics::Collision* first_coll = l_it->GetCollisions()[0].get();
                                particles_coll_map[first_coll] = false;
                            }

                            // map the particles model to the untransfered links
                            // set map< model - map< link - bool > > to FALSE
                            this->transferedParticlesWFromLocation[this->world->GetModel(p_it)] = particles_coll_map;
                        }

                        // map container collision to all particle model vector
                        this->contCollToPModelsWFromLocation[c_iter.get()] = particle_models;
                    }

                    // check if tool collision type
                    const auto tool_res = this->toolCollToPNames.find(c_iter->GetName());

                    if (tool_res != this->toolCollToPNames.end())
                    {
                        // particle models
                        std::set< gazebo::physics::ModelPtr> particle_models;

                        // loop through particle names to find the models and add them to the vector
                        for (const auto p_it : tool_res->second)
                        {
                            // TODO don't segault if not in world
                            // get the particle model
                            physics::ModelPtr particle_model = this->world->GetModel(p_it);

                            // insert model to the set
                            particle_models.insert(particle_model);
                        }
                        
                        // add the collision to the event map
                        this->nameToEventObj_M[c_iter->GetName()] = new PpEventObj(c_iter->GetName(),"knowrob_sim:","toolPart");
                        
                        // map collision name to the beliefstate object
                        this->nameToBsObject_M[c_iter->GetName()] =
                                new beliefstate_client::Object("&knowrob_sim;", c_iter->GetName());
                                
                        // map tool collision to all particle model vector
                        this->toolCollToPModels[c_iter.get()] = particle_models;
                    }
                    
                    // check if tool part collision type
                    const auto tool_part_res = this->toolCollPartToModelNames.find(c_iter->GetName());

                    if (tool_part_res != this->toolCollPartToModelNames.end())
                    {
                        // particle models
                        std::set< gazebo::physics::ModelPtr> models;

                        // loop through model names to find the models and add them to the vector
                        for (const auto p_it : tool_part_res->second)
                        {
                            // TODO don't segault if not in world
                            // get the particle model
                            physics::ModelPtr model = this->world->GetModel(p_it);
                            
                            // insert model to the set
                            models.insert(model);
                        }
                        
                        // add the collision to the event map
                        this->nameToEventObj_M[c_iter->GetName()] = new PpEventObj(c_iter->GetName(),"knowrob_sim:","toolPart");
                        
                        // map collision name to the beliefstate object
                        this->nameToBsObject_M[c_iter->GetName()] =
                                new beliefstate_client::Object("&knowrob_sim;", c_iter->GetName());
                                
                        // map tool collision to all particle model vector
                        this->toolCollPartToModels[c_iter.get()] = models;
                    }
                }
            }
        }
    }
    
    // add custom embedded events in the log file
    if (this->customEmbeddedEv)
    {
        LogEvents::CheckCustomEmbeddedEv();
    }  
}

//////////////////////////////////////////////////
void LogEvents::CheckCustomEmbeddedEv()
{    
    // open file and seek to the end
    std::ifstream log_file;
    log_file.open(util::LogPlay::Instance()->FullPathFilename(), std::ios_base::ate);
    
    // store the custom events in chronological order
    std::stack<PpEvent*> custom_events_stack;
    
    // file length
    unsigned long int length;
    char c = '\0';
    
    // events ss from the end of the file
    std::stringstream ev_ss;
                        
    if(log_file)
    {
        ev_ss << "<custom_events>";
        // get file length
        length = log_file.tellg();
        
        // iterate backwards
        for (unsigned long int i = length - 2; i > 0; i--)
        {
            // get the current character
            log_file.seekg(i);
            c = log_file.get();
                        
            // check if the character end of line
            if (c == '\r' || c == '\n')
            {
                // get the whole line
                std::string curr_line;       
                std::getline(log_file, curr_line);
                // stop when the custom events end, or if there aren't any
                if (curr_line == "<custom_events>" || curr_line == "</gazebo_log>")
                {
                    break;
                }
                // only add <event> tags
                else if (curr_line != "</custom_events>")
                {
                    ev_ss << curr_line << "\n";
                }     
            }
        }        
        ev_ss << "</custom_events>";
    }    
    // close file
    log_file.close();
    
    // parse the custom events tags as xml document
    tinyxml2::XMLDocument doc;    
    doc.Parse(ev_ss.str().c_str());

    // get and iterate through the custom events
    tinyxml2::XMLElement* xml_custom_events = doc.FirstChildElement("custom_events");    
    for(tinyxml2::XMLElement* ev = xml_custom_events->FirstChildElement(); 
        ev != NULL; ev = ev->NextSiblingElement())
    {
        // get the text from the xml doc
        std::stringstream ev_ss(ev->GetText());
        
        // vector of the items
        std::vector<std::string> ev_items;
        std::string ev_item;
        
        // add the event items to the vector
        while(std::getline(ev_ss, ev_item, ',')) 
        {
            ev_items.push_back(ev_item);
        }
        
        // get event type and timestamp
        const std::string ev_type = ev_items.front();
        
        // create gz events from the read ev_items
        if(ev_type == "TurningOnPoweredDevice")
        {   
            // timestamp (last item)
            const double start_ts = std::stod(ev_items.back());
            // end timestamp (last item)
            const double end_ts = start_ts + 0.1;
            
            // event name
            const std::string curr_ev_name = ev_type + "-" +  ev_items.at(1);
            
            // create the contact GzEvent
            PpEvent* curr_pp_event = new PpEvent(
                    curr_ev_name, "&knowrob;", ev_type,
                    "knowrob:", "objectActedOn", start_ts);
            
            // add object acted on
            curr_pp_event->AddObject(
                        this->nameToEventObj_M[ev_items.at(1)]);
               
            // end of the event
            curr_pp_event->End(end_ts);
  
            // add event to the back
            custom_events_stack.push(curr_pp_event);
        }
        else if (ev_type == "TurningOffPoweredDevice")
        {
            // timestamp (last item)
            const double start_ts = std::stod(ev_items.back());
            // end timestamp (last item)
            const double end_ts = start_ts + 0.1;
            
            // event name
            const std::string curr_ev_name = ev_type + "-" +  ev_items.at(1);
            
            // create the contact GzEvent
            PpEvent* curr_pp_event = new PpEvent(
                    curr_ev_name, "&knowrob;", ev_type,
                    "knowrob:", "objectActedOn", start_ts);

            // add object acted on
            curr_pp_event->AddObject(
                        this->nameToEventObj_M[ev_items.at(1)]);
            
            // end of the event
            curr_pp_event->End(end_ts);
    
            // add event to the back
            custom_events_stack.push(curr_pp_event);
        }
        else if (ev_type == "Pipetting")
        {
            // start timestamp (second to last)
            const double start_ts = std::stod(ev_items.at(ev_items.size()-2));
            // end timestamp (last item)
            const double end_ts = std::stod(ev_items.back());
            
            // event name
            const std::string curr_ev_name = ev_type + "-" + ev_items.at(1);
            
            // create the contact GzEvent
            PpEvent* curr_pp_event = new PpEvent(
                    curr_ev_name, "&knowrob;", ev_type,
                    "knowrob:", "objectActedOn", start_ts);
           
            // add object acted on
            curr_pp_event->AddObject(
                        this->nameToEventObj_M[ev_items.at(1)]);
            
            // end of the event
            curr_pp_event->End(end_ts);
                 
            // add event to the back
           custom_events_stack.push(curr_pp_event);
        }
        else if (ev_type == "adt")
        {
            pracAdtObj.insert(std::tuple<std::string, std::string>(ev_items.at(1),LogEvents::ToCamelCase(ev_items.at(2))));
        }
    }
   
    // add events in the chronological order to the events map
    while(!custom_events_stack.empty())
    {
        PpEvent* e = custom_events_stack.top();
        custom_events_stack.pop();
        this->nameToEvents_M[e->GetName()].push_back(e);
        
        std::cout << "*LogEvents* - Start - \t" << e->GetName() << "\t\t\t at " << e->GetStartTime()  << std::endl;
        std::cout << "*LogEvents* - End - \t" << e->GetName() << "\t\t\t at " << e->GetEndTime()  << std::endl;
    }
}

//////////////////////////////////////////////////
void LogEvents::CheckEvents()
{
    // compute simulation time in milliseconds
    const double timestamp_ms = this->world->GetSimTime().Double();

    // state of models being in contact with surfaces
    // <surface model name, model in contact with>
    std::set<std::pair<std::string, std::string> > surface_models_in_contacts;

    // nr of contacts with the grasping 'sensors'
    // grasping happens if all 'sensors' have a contact ( nr_grasp_contacts = graspColl.size )
    unsigned int grasp_contacts_nr = 0;
    unsigned int l_grasp_contacts_nr = 0;

    // models in contact with grasping 'sensors',
    // grasping happens if all models are the same (e.g. set size = 1)
    std::set<std::string> grasped_models;
    std::set<std::string> l_grasped_models;

    // TODO not generalized
    // model in contact with the tool
    std::string contact_with_tool_model;
    std::string tool_name;
    std::string tool_collision_name;
    
    // model in contact with the tool part
    std::string contact_with_tool_part_model;
    std::string tool_part_name;
    std::string tool_part_collision_name;

    // flag for detecting particle correct transfer
    bool transf_detect_flag = false;
    
     // flag for detecting particle correct transfer with goal location
    bool transf_detect_flag_gl = false; 
    std::string transf_goal_model_name;
    
     // flag for detecting particle correct transfer with from location
    bool transf_detect_flag_fl = false;  
    std::string transf_from_model_name;
        
    // flag for detecting model correct transfer
    bool model_transf_detect_flag = false;

    ////////////// Iterate through all current contacts
    // By iterating through all the contacts we check their types
    // and compare them to the prev state to see if changes occured
    for(const auto c_it : this->contactManagerPtr->GetContacts())
    {
        // get the collisions in contact
        physics::Collision* c_coll1 = c_it->collision1;
        physics::Collision* c_coll2 = c_it->collision2;


        ////////////// Supporting Collisions
        /// add model names contact with a surface collision ('sensor')
        /// Set(<model1_name, model2_name>, [..])

        // check if the contact collision pair (first, second) are in contact with a surface 'sensor'
        if (this->surfaceColls.find(c_coll1) != this->surfaceColls.end())
        {
            // add the model names pair to the set
            surface_models_in_contacts.insert(std::pair<std::string, std::string>(
                    c_coll1->GetParentModel()->GetName(),
                    c_coll2->GetParentModel()->GetName()));
        }
        else if(this->surfaceColls.find(c_coll2) != this->surfaceColls.end())
        {
            // add the model names pair to the set
            surface_models_in_contacts.insert(std::pair<std::string, std::string>(
                    c_coll2->GetParentModel()->GetName(),
                    c_coll1->GetParentModel()->GetName()));
        }


        /////////////// Grasping
        /// check if any of the collision pair is in contact with a grasping 'sensor',
        /// if in contact, increment the grasping contact number
        /// and add the model to the grasped models set,
        /// if the contact nr = nr sensors, and all models in contact with = 1, then we have a grasp

        // check any of the collisions pair is in contact with a grasping 'sensor'
        if (this->graspColls.find(c_coll1) != this->graspColls.end())
        {
            // increase the current number of grasp contacts
            grasp_contacts_nr++;

            // add model to the grasped objects
            // notice the model is the opposite from the checked collision
            grasped_models.insert(c_coll2->GetParentModel()->GetName());

        }
        else if(this->graspColls.find(c_coll2) != this->graspColls.end())
        {
            // increase the current number of grasp contacts
            grasp_contacts_nr++;

            // add model to the grasped objects
            // notice the model is the opposite from the checked collision
            grasped_models.insert(c_coll1->GetParentModel()->GetName());
        }
    
        // Left grasp
        // check any of the collisions pair is in contact with a grasping 'sensor'
        if (this->l_graspColls.find(c_coll1) != this->l_graspColls.end())
        {
            // increase the current number of grasp contacts
            l_grasp_contacts_nr++;

            // add model to the grasped objects
            // notice the model is the opposite from the checked collision
            l_grasped_models.insert(c_coll2->GetParentModel()->GetName());

        }
        else if(this->l_graspColls.find(c_coll2) != this->l_graspColls.end())
        {
            // increase the current number of grasp contacts
            l_grasp_contacts_nr++;

            // add model to the grasped objects
            // notice the model is the opposite from the checked collision
            l_grasped_models.insert(c_coll1->GetParentModel()->GetName());
        }
        

        /////////////// Translation (Particles leaving container)
        /// Check if one of the collision is the container top 'sensor',
        /// if yes, check if the sensor is in contact with one of the related particles
        /// e.g. `mug_top_ev` coll is in collision with `Cheese` or `Sauce` etc. Model

        // check any of the collisions pair is in contact with a container top 'sensor'
        // get the results for finding the collisions in the map
        const auto coll1_to_model_res = this->contCollToPModels.find(c_coll1);
        const auto coll2_to_model_res = this->contCollToPModels.find(c_coll2);

        // c_coll2 is the particle in contact with the container sensor
        // if results were found check if they are in contact with the related particle model
        if (coll1_to_model_res != this->contCollToPModels.end())
        {
            // check if the object in collision with the required particle type
            // ->second is the set of the particle models
            const auto coll1_model =
                    coll1_to_model_res->second.find(c_coll2->GetParentModel());

            // if the model belongs to the related set
            if (coll1_model != coll1_to_model_res->second.end())
            {
                // if particle has not been transfered yet, increment counter and set flag to true
                // save the timestamp of the particle leaving
                if(!this->transferedParticles[(*coll1_model)][c_coll2])
                {
                    // increment counter
                    this->transferedPartCount[(*coll1_model)]++;

                    // set flag to transfered (true)
                    this->transferedParticles[(*coll1_model)][c_coll2] = true;

                    // save particle transfer timestamp
                    this->transfTs = timestamp_ms;

                    // set flag to true
                    transf_detect_flag = true;

                    // add the particle collision to the transf event pool
                    this->transfParticlePool.push_back(c_coll2);
                }
            }
        }
        // c_coll1 - particle in collision with the container sensor
        else if (coll2_to_model_res != this->contCollToPModels.end())
        {
            // check if the object in collision with the required particle type
            // ->second is the set of the particle models
            const auto coll2_model =
                    coll2_to_model_res->second.find(c_coll1->GetParentModel());

            // if the model belongs to the related set
            if (coll2_model != coll2_to_model_res->second.end())
            {
                // if particle has not been transfered yet, increment counter and set flag to true
                // save the timestamp of the particle leaving
                if(!this->transferedParticles[(*coll2_model)][c_coll1])
                {
                    // increment counter
                    this->transferedPartCount[(*coll2_model)]++;

                    // set flag to transfered (true)
                    this->transferedParticles[(*coll2_model)][c_coll1] = true;

                    // save particle transfer timestamp
                    this->transfTs = timestamp_ms;

                    // set flag to true
                    transf_detect_flag = true;

                    // add the particle collision to the transf event pool
                    this->transfParticlePool.push_back(c_coll1);
                }
            }
        }

        
        /////////////// Translation (Particles leaving container) with goal location
        /// Check if one of the collision is the container top 'sensor',
        /// if yes, check if the sensor is in contact with one of the related particles
        /// e.g. `mug_top_ev` coll is in collision with `Cheese` or `Sauce` etc. Model

        // check any of the collisions pair is in contact with a container top 'sensor'
        // get the results for finding the collisions in the map
        const auto coll1_to_model_res_gl = this->contCollToPModelsWGoalLocation.find(c_coll1);
        const auto coll2_to_model_res_gl = this->contCollToPModelsWGoalLocation.find(c_coll2);

        // c_coll2 is the particle in contact with the container sensor
        // if results were found check if they are in contact with the related particle model
        if (coll1_to_model_res_gl != this->contCollToPModelsWGoalLocation.end())
        {            
            // name of the transfer goal model
            transf_goal_model_name = c_coll1->GetParentModel()->GetName();
            
            // check if the object in collision with the required particle type
            // ->second is the set of the particle models
            const auto coll1_model =
                    coll1_to_model_res_gl->second.find(c_coll2->GetParentModel());

            // if the model belongs to the related set
            if (coll1_model != coll1_to_model_res_gl->second.end())
            {
                // if particle has not been transfered yet, increment counter and set flag to true
                // save the timestamp of the particle leaving
                if(!this->transferedParticlesWGoalLocation[(*coll1_model)][c_coll2])
                {
                    // increment counter
                    this->transferedPartCountWGoalLocation[(*coll1_model)]++;

                    // set flag to transfered (true)
                    this->transferedParticlesWGoalLocation[(*coll1_model)][c_coll2] = true;

                    // save particle transfer timestamp
                    this->transfTsWGoalLocation = timestamp_ms;

                    // set flag to true
                    transf_detect_flag_gl = true;

                    // add the particle collision to the transf event pool
                    this->transfParticlePoolWGoalLocation.push_back(c_coll2);
                }
            }
        }
        // c_coll1 - particle in collision with the container sensor
        else if (coll2_to_model_res_gl != this->contCollToPModelsWGoalLocation.end())
        {
            // name of the transfer goal model
            transf_goal_model_name = c_coll2->GetParentModel()->GetName();
            
            // check if the object in collision with the required particle type
            // ->second is the set of the particle models
            const auto coll2_model =
                    coll2_to_model_res_gl->second.find(c_coll1->GetParentModel());

            // if the model belongs to the related set
            if (coll2_model != coll2_to_model_res_gl->second.end())
            {
                // if particle has not been transfered yet, increment counter and set flag to true
                // save the timestamp of the particle leaving
                if(!this->transferedParticlesWGoalLocation[(*coll2_model)][c_coll1])
                {
                    // increment counter
                    this->transferedPartCountWGoalLocation[(*coll2_model)]++;

                    // set flag to transfered (true)
                    this->transferedParticlesWGoalLocation[(*coll2_model)][c_coll1] = true;

                    // save particle transfer timestamp
                    this->transfTsWGoalLocation = timestamp_ms;

                    // set flag to true
                    transf_detect_flag_gl = true;

                    // add the particle collision to the transf event pool
                    this->transfParticlePoolWGoalLocation.push_back(c_coll1);
                }
            }
        }
        
        /////////////// Translation (Particles leaving container) with from location
        /// Check if one of the collision is the container top 'sensor',
        /// if yes, check if the sensor is in contact with one of the related particles
        /// e.g. `mug_top_ev` coll is in collision with `Cheese` or `Sauce` etc. Model

        // check any of the collisions pair is in contact with a container top 'sensor'
        // get the results for finding the collisions in the map
        const auto coll1_to_model_res_fl = this->contCollToPModelsWFromLocation.find(c_coll1);
        const auto coll2_to_model_res_fl = this->contCollToPModelsWFromLocation.find(c_coll2);

        // c_coll2 is the particle in contact with the container sensor
        // if results were found check if they are in contact with the related particle model
        if (coll1_to_model_res_fl != this->contCollToPModelsWFromLocation.end())
        {
            // name of the transfer from model
            transf_from_model_name = c_coll1->GetParentModel()->GetName();
            
            // check if the object in collision with the required particle type
            // ->second is the set of the particle models
            const auto coll1_model =
                    coll1_to_model_res_fl->second.find(c_coll2->GetParentModel());

            // if the model belongs to the related set
            if (coll1_model != coll1_to_model_res_fl->second.end())
            {
                // if particle has not been transfered yet, increment counter and set flag to true
                // save the timestamp of the particle leaving
                if(!this->transferedParticlesWFromLocation[(*coll1_model)][c_coll2])
                {
                    // increment counter
                    this->transferedPartCountWFromLocation[(*coll1_model)]++;

                    // set flag to transfered (true)
                    this->transferedParticlesWFromLocation[(*coll1_model)][c_coll2] = true;

                    // save particle transfer timestamp
                    this->transfTsWFromLocation = timestamp_ms;

                    // set flag to true
                    transf_detect_flag_fl = true;

                    // add the particle collision to the transf event pool
                    this->transfParticlePoolWFromLocation.push_back(c_coll2);
                }
            }
        }
        // c_coll1 - particle in collision with the container sensor
        else if (coll2_to_model_res_fl != this->contCollToPModelsWFromLocation.end())
        {
            // name of the transfer from model
            transf_from_model_name = c_coll2->GetParentModel()->GetName();
            
            // check if the object in collision with the required particle type
            // ->second is the set of the particle models
            const auto coll2_model =
                    coll2_to_model_res_fl->second.find(c_coll1->GetParentModel());

            // if the model belongs to the related set
            if (coll2_model != coll2_to_model_res_fl->second.end())
            {
                // if particle has not been transfered yet, increment counter and set flag to true
                // save the timestamp of the particle leaving
                if(!this->transferedParticlesWFromLocation[(*coll2_model)][c_coll1])
                {
                    // increment counter
                    this->transferedPartCountWFromLocation[(*coll2_model)]++;

                    // set flag to transfered (true)
                    this->transferedParticlesWFromLocation[(*coll2_model)][c_coll1] = true;

                    // save particle transfer timestamp
                    this->transfTsWFromLocation = timestamp_ms;

                    // set flag to true
                    transf_detect_flag_fl = true;

                    // add the particle collision to the transf event pool
                    this->transfParticlePoolWFromLocation.push_back(c_coll1);
                }
            }
        }
        
        /////////////// Tool (Particles in contact with tool)
        /// Check if one of the collision is the tool coll 'sensor',
        /// if yes, check if the sensor is in contact with one of the related particles
        /// e.g. `spoon_head_coll` coll is in collision with `Cheese` or `Sauce` etc. Model

        // check any of the collisions pair is in contact with a container top 'sensor'
        // get the results for finding the collisions in the map
        const auto tool_coll1_to_model_res = this->toolCollToPModels.find(c_coll1);
        const auto tool_coll2_to_model_res = this->toolCollToPModels.find(c_coll2);

        // c_coll2 is the particle in contact with the container sensor
        // if results were found check if they are in contact with the related particle model
        if (tool_coll1_to_model_res != this->toolCollToPModels.end())
        {
            // check if the object in collision with the required particle type
            // ->second is the set of the particle models
            const auto coll1_model =
                    tool_coll1_to_model_res->second.find(c_coll2->GetParentModel());

            // TODO this just takes the last coll
            // if the model belongs to the related set
            if (coll1_model != tool_coll1_to_model_res->second.end())
            {
                contact_with_tool_model = c_coll2->GetParentModel()->GetName();
                tool_name = c_coll1->GetParentModel()->GetName();
                tool_collision_name = c_coll1->GetName();
            }
        }
        // c_coll1 - particle in collision with the container sensor
        else if (tool_coll2_to_model_res != this->toolCollToPModels.end())
        {
            // check if the object in collision with the required particle type
            // ->second is the set of the particle models
            const auto coll2_model =
                    tool_coll2_to_model_res->second.find(c_coll1->GetParentModel());

            // TODO this just takes the last coll
            // if the model belongs to the related set
            if (coll2_model != tool_coll2_to_model_res->second.end())
            {
                contact_with_tool_model = c_coll1->GetParentModel()->GetName();
                tool_name = c_coll2->GetParentModel()->GetName();
                tool_collision_name = c_coll2->GetName();
            }
        }
        
        
        /////////////// Tool part collision (Object part colliding with other model)
        /// Check if one of the collision is the tool coll 'sensor',
        /// if yes, check if the sensor is in contact with one of the related particles
        /// e.g. `spoon_head_coll` coll is in collision with `Cheese` or `Sauce` etc. Model

        // check any of the collisions pair is in contact with a tool 'sensor'
        // get the results for finding the collisions in the map
        const auto tool_part_coll1_to_model_res = this->toolCollPartToModels.find(c_coll1);
        const auto tool_part_coll2_to_model_res = this->toolCollPartToModels.find(c_coll2);

        // c_coll2 is the particle in contact with the container sensor
        // if results were found check if they are in contact with the related particle model
        if (tool_part_coll1_to_model_res != this->toolCollPartToModels.end())
        {
            // check if the object in collision with the required particle type
            // ->second is the set of the particle models
            const auto coll1_model =
                    tool_part_coll1_to_model_res->second.find(c_coll2->GetParentModel());

            // TODO this just takes the last coll
            // if the model belongs to the related set
            if (coll1_model != tool_part_coll1_to_model_res->second.end())
            {
                contact_with_tool_part_model = c_coll2->GetParentModel()->GetName();
                tool_part_name = c_coll1->GetParentModel()->GetName();
                tool_part_collision_name = c_coll1->GetName();
            }
        }
        // c_coll1 - particle in collision with the container sensor
        else if (tool_part_coll2_to_model_res != this->toolCollPartToModels.end())
        {
            // check if the object in collision with the required particle type
            // ->second is the set of the particle models
            const auto coll2_model =
                    tool_part_coll2_to_model_res->second.find(c_coll1->GetParentModel());

            // TODO this just takes the last coll
            // if the model belongs to the related set
            if (coll2_model != tool_part_coll2_to_model_res->second.end())
            {
                contact_with_tool_part_model = c_coll1->GetParentModel()->GetName();
                tool_part_name = c_coll2->GetParentModel()->GetName();
                tool_part_collision_name = c_coll2->GetName();
            }
        }
        
        
        ////////////// Translation (Model leaving/entering container)
        // check if the contact collision pair (first, second) are in contact with a container top 'sensor'
        if (this->modelTranslColls.find(c_coll1) != this->modelTranslColls.end())
        {           
            // model being transfered
            this->transferedModel = c_coll2->GetParentModel();

            // set flag to true
            model_transf_detect_flag = true;
        }
        else if(this->modelTranslColls.find(c_coll2) != this->modelTranslColls.end())
        {
            // set flag to true
            model_transf_detect_flag = true;
            
            // model being transfered
            this->transferedModel = c_coll1->GetParentModel();
        }        
    }

    /////////////// Compare current state with previous one

    // Check if difference between the supporting events have appeared
    LogEvents::CheckSurfaceEvents(
                timestamp_ms, surface_models_in_contacts);

    // Check if the grasp has changed
    LogEvents::CheckGraspEvent(
                timestamp_ms, grasp_contacts_nr, grasped_models);

    // Check if the left grasp has changed
    LogEvents::LCheckGraspEvent(
                timestamp_ms, l_grasp_contacts_nr, l_grasped_models);
    
    // Check transfer event
    LogEvents::CheckTranslEvent(
                timestamp_ms, transf_detect_flag);

    // Check transfer event with goal location
    LogEvents::CheckTranslEventWGoalLocation(
                timestamp_ms, transf_detect_flag_gl, transf_goal_model_name);
    
        // Check transfer event with from location
    LogEvents::CheckTranslEventWFromLocation(
                timestamp_ms, transf_detect_flag_fl, transf_from_model_name);
    
    // Check tool event
    LogEvents::CheckToolEvent(
                timestamp_ms, contact_with_tool_model, tool_name, tool_collision_name);
  
    // Check tool part event
    LogEvents::CheckToolPartEvent(
                timestamp_ms, contact_with_tool_part_model, tool_part_name, tool_part_collision_name);
    
    // Check model transfer event
    LogEvents::CheckModelTranslEvent(
                timestamp_ms, model_transf_detect_flag);

}

//////////////////////////////////////////////////
void LogEvents::FiniEvents()
{
    // close main GzEvent
    this->nameToEvents_M["Episode-Duration"].back()->End(this->world->GetSimTime().Double());

    // close all open events
    LogEvents::EndActiveEvents();

    // Concatenate timelines with short disconnections
    LogEvents::MergeEventDisconnections();
    
    // Remove grasping jitters
    LogEvents::RemoveGraspJitters();

    // write events as belief state contexts
    LogEvents::WriteContexts();

    // write events to timeline file
    LogEvents::WriteTimelines();
}

//////////////////////////////////////////////////
void LogEvents::CheckGraspEvent(
        const double _timestamp_ms,
        const unsigned int _grasp_contacts_nr,
        const std::set<std::string> &_grasped_models)
{
    // current grasped model
    std::string curr_grasped_model;

    // check for grasp nr contacts eq the total grasp 'sensors'
    // and all of them are in contact with the same model
    if(_grasp_contacts_nr == this->graspColls.size() && _grasped_models.size() == 1)
    {
        // TODO only checks for the first model?
        curr_grasped_model = (*_grasped_models.begin());
    }

    // check for difference between current and prev grasp
    if (curr_grasped_model != this->prevGraspedModel)
    {
        if (!curr_grasped_model.empty())
        {
            this->prevGraspedModel = curr_grasped_model;

            // the name of the grasping event
            std::string grasp_ev_name = "Grasp-" + curr_grasped_model;

            // TODO why is if needed here? the two parts seem similar
            // check if the grasp event has been initialized
            if(this->graspGzEvent != NULL)
            {
                // create the contact GzEvent
                this->graspGzEvent = new PpEvent(
                        grasp_ev_name, "&knowrob;", "GraspingSomething",
                        "knowrob:", "objectActedOn", _timestamp_ms);

                // add grasped object
                this->graspGzEvent->AddObject(
                            this->nameToEventObj_M[curr_grasped_model]);

                std::cout << "*LogEvents* - Start - \t" << grasp_ev_name << "\t\t\t at " << _timestamp_ms  << std::endl;
            }
            // init first grasp
            else
            {
                // create first grasp GzEvent
                this->graspGzEvent = new PpEvent(
                        grasp_ev_name, "&knowrob;", "GraspingSomething",
                        "knowrob:", "objectActedOn", _timestamp_ms);

                // add grasped object
                this->graspGzEvent->AddObject(
                            this->nameToEventObj_M[curr_grasped_model]);

                std::cout << "*LogEvents* - Init - \t" << grasp_ev_name << "\t\t\t at " << _timestamp_ms  << std::endl;
            }
        }
        else
        {
            // delete name
            this->prevGraspedModel.clear();

            // end grasping event
            this->graspGzEvent->End(_timestamp_ms);

            //TODO in order to remove the this->graspGzEvent != NULL check, we can delete this->graspevent and set it to NULL
            // similarly to transl events (or the other way around)

            std::cout << "*LogEvents* - End - \t" << this->graspGzEvent->GetName() << "\t\t\t at " << _timestamp_ms  << std::endl;

            // add grasp event to the map of all events
            this->nameToEvents_M[this->graspGzEvent->GetName()].push_back(this->graspGzEvent);
        }
    }
}

//////////////////////////////////////////////////
void LogEvents::LCheckGraspEvent(
        const double _timestamp_ms,
        const unsigned int _grasp_contacts_nr,
        const std::set<std::string> &_grasped_models)
{
    // current grasped model
    std::string curr_grasped_model;

    // check for grasp nr contacts eq the total grasp 'sensors'
    // and all of them are in contact with the same model
    if(_grasp_contacts_nr == this->l_graspColls.size() && _grasped_models.size() == 1)
    {
        // TODO only checks for the first model?
        curr_grasped_model = (*_grasped_models.begin());
    }

    // check for difference between current and prev grasp
    if (curr_grasped_model != this->l_prevGraspedModel)
    {
        if (!curr_grasped_model.empty())
        {
            this->l_prevGraspedModel = curr_grasped_model;

            // the name of the grasping event
            std::string grasp_ev_name = "LGrasp-" + curr_grasped_model;

            // TODO why is if needed here? the two parts seem similar
            // check if the grasp event has been initialized
            if(this->l_graspGzEvent != NULL)
            {
                // create the contact GzEvent
                this->l_graspGzEvent = new PpEvent(
                        grasp_ev_name, "&knowrob;", "GraspingSomething",
                        "knowrob:", "objectActedOn", _timestamp_ms);

                // add grasped object
                this->l_graspGzEvent->AddObject(
                            this->nameToEventObj_M[curr_grasped_model]);

                std::cout << "*LogEvents* - Start - \t" << grasp_ev_name << "\t\t\t at " << _timestamp_ms  << std::endl;
            }
            // init first grasp
            else
            {
                // create first grasp GzEvent
                this->l_graspGzEvent = new PpEvent(
                        grasp_ev_name, "&knowrob;", "GraspingSomething",
                        "knowrob:", "objectActedOn", _timestamp_ms);

                // add grasped object
                this->l_graspGzEvent->AddObject(
                            this->nameToEventObj_M[curr_grasped_model]);

                std::cout << "*LogEvents* - Init - \t" << grasp_ev_name << "\t\t\t at " << _timestamp_ms  << std::endl;
            }
        }
        else
        {
            // delete name
            this->l_prevGraspedModel.clear();

            // end grasping event
            this->l_graspGzEvent->End(_timestamp_ms);

            //TODO in order to remove the this->graspGzEvent != NULL check, we can delete this->graspevent and set it to NULL
            // similarly to transl events (or the other way around)

            std::cout << "*LogEvents* - End - \t" << this->l_graspGzEvent->GetName() << "\t\t\t at " << _timestamp_ms  << std::endl;

            // add grasp event to the map of all events
            this->nameToEvents_M[this->l_graspGzEvent->GetName()].push_back(this->l_graspGzEvent);
        }
    }
}

//////////////////////////////////////////////////
void LogEvents::CheckSurfaceEvents(
        const double _timestamp_ms,
        const std::set<std::pair<std::string, std::string>> &_curr_surface_models_in_contact)
{
    // the symmetric difference will be added to this set
    std::set<std::pair<std::string, std::string> > symmetric_diff_S;

    // computing the symmetric diff between the curr and prev set
    std::set_symmetric_difference(
            _curr_surface_models_in_contact.begin(),
            _curr_surface_models_in_contact.end(),
            this->prevSurfaceModelsInContact.begin(),
            this->prevSurfaceModelsInContact.end(),
            std::inserter(symmetric_diff_S,symmetric_diff_S.end()));

    // check if there are any differences
    if (symmetric_diff_S.size() > 0)
    {
        // set the prev values to the current one
        this->prevSurfaceModelsInContact = _curr_surface_models_in_contact;

        // iterate through all the collising event models
        for(std::set<std::pair<std::string, std::string> >::const_iterator m_iter = symmetric_diff_S.begin();
                m_iter != symmetric_diff_S.end(); m_iter++)
        {
            // set name of the context first models + second model in contact
            std::string contact_ev_name = "Contact-" + m_iter->first + "-" + m_iter->second;


            // if event does not exist
            // TODO add two way detection of contacts, store somewhere the pairs in contact
            if(!this->nameToEvents_M.count(contact_ev_name))
            {
                // create local contact GzEvent
                sg_pp::PpEvent* contact_event = new sg_pp::PpEvent(
                        contact_ev_name, "&knowrob_sim;", "TouchingSituation",
                        "knowrob_sim:", "inContact", _timestamp_ms);

                // add the two objects in contact
                contact_event->AddObject(this->nameToEventObj_M[m_iter->first]);

                contact_event->AddObject(this->nameToEventObj_M[m_iter->second]);

                // add local event to the map
                this->nameToEvents_M[contact_ev_name].push_back(contact_event);

                std::cout << "*LogEvents* - Start - \t" << contact_ev_name << "\t\t at "
                          << _timestamp_ms << std::endl;
            }
            else
            {
                // if the contact exists and it is open, end it
                if(this->nameToEvents_M[contact_ev_name].back()->IsOpen())
                {
                    // end contact event
                    this->nameToEvents_M[contact_ev_name].back()->End(_timestamp_ms);

                    std::cout << "*LogEvents* - End - \t" << contact_ev_name << "\t\t at "
                              << _timestamp_ms << std::endl;
                }
                else
                {
                    // create local contact GzEvent
                    sg_pp::PpEvent* contact_event = new sg_pp::PpEvent(
                            contact_ev_name, "&knowrob_sim;", "TouchingSituation",
                            "knowrob_sim:", "inContact", _timestamp_ms);

                    // add the two objects in contact
                    contact_event->AddObject(this->nameToEventObj_M[m_iter->first]);

                    contact_event->AddObject(this->nameToEventObj_M[m_iter->second]);

                    // add local event to the map
                    this->nameToEvents_M[contact_ev_name].push_back(contact_event);

                    std::cout << "*LogEvents* - Start - \t" << contact_ev_name << "\t\t at "
                              << _timestamp_ms << std::endl;
                }
            }
        }
    }
}

//////////////////////////////////////////////////
void LogEvents::CheckTranslEvent(
        const double _timestamp_ms,
        const bool _transf_detect_flag)
{
    // check if new transfer was detected
    // if transl ev not init, create event
    // TODO add init to events
    if(_transf_detect_flag && this->transfEvent == NULL)
    {
        // TODO check for multiple particle types
        // save type of first particle from pool
        std::string particle_type = (*this->transfParticlePool.begin())->GetParentModel()->GetName();

        // TODO property namespace, and porperty should belong to the OBJECT
        // create the contact GzEvent
        this->transfEvent = new PpEvent(
                    "ParticleTranslation-" + particle_type, "&knowrob_sim;", "ParticleTranslation",
                    "knowrob:", "particleType", _timestamp_ms);

        this->transfEvent->AddObject(this->nameToEventObj_M[particle_type]);
        //this->translEvent->AddObject(new GzEventObj(particle_type));

        std::cout << "*LogEvents* - Start - \t" << this->transfEvent->GetName()
                  << " [" << particle_type << "]"
                  << "\t\t at " << _timestamp_ms << std::endl;
    }

    if((_timestamp_ms - this->transfTs > this->transfEvDurThresh) && this->transfEvent != NULL)
    {
        // TODO hack for getting the nr of particles
        // add the nr of particles scooped
        //this->translEvent->AddObject(
        //               new GzEventObj(std::to_string(this->transfParticlePool.size())));

        // finish gz event
        // end grasping event
        this->transfEvent->End(this->transfTs);

        std::cout << "*LogEvents* - End - \t" << this->transfEvent->GetName()
                  << " [" << this->transfParticlePool.size() << "]"
                  << "\t\t at " << this->transfTs << std::endl;

        // add grasp event to the map of all events
        this->nameToEvents_M[this->transfEvent->GetName()].push_back(this->transfEvent);

        // set pointer to NULL, do not delete it, since later we will access the memory using the saved events
        this->transfEvent = NULL;
        this->transfParticlePool.clear();
    }
}

//////////////////////////////////////////////////
void LogEvents::CheckTranslEventWGoalLocation(
        const double _timestamp_ms,
        const bool _transf_detect_flag,
        const std::string _transf_goal_model_name)
{
    // check if new transfer was detected
    // if transl ev not init, create event
    // TODO add init to events
    if(_transf_detect_flag && this->transfEventWGoalLocation == NULL)
    {
        // TODO check for multiple particle types
        // save type of first particle from pool
        std::string particle_type = (*this->transfParticlePoolWGoalLocation.begin())->GetParentModel()->GetName();

        // TODO property namespace, and porperty should belong to the OBJECT
        // create the contact GzEvent
        this->transfEventWGoalLocation = new PpEvent(
                    "ParticleTranslation-" + particle_type + "-Goal-" + _transf_goal_model_name,
                    "&knowrob_sim;", "ParticleTranslation",
                    "knowrob:", "particleType", _timestamp_ms);

        this->transfEventWGoalLocation->AddObject(this->nameToEventObj_M[particle_type]);
        this->transfEventWGoalLocation->AddObject(new PpEventObj(_transf_goal_model_name,
            "knowrob:","goalLocation"));

        std::cout << "*LogEvents* - Start - \t" << this->transfEventWGoalLocation->GetName()
                  << " [" << particle_type << "]"
                  << "\t\t at " << _timestamp_ms << std::endl;
    }

    if((_timestamp_ms - this->transfTsWGoalLocation > this->transfEvDurThresh) && this->transfEventWGoalLocation != NULL)
    {
        // TODO hack for getting the nr of particles
        // add the nr of particles scooped
        //this->translEvent->AddObject(
        //               new GzEventObj(std::to_string(this->transfParticlePool.size())));

        // finish gz event
        // end grasping event
        this->transfEventWGoalLocation->End(this->transfTsWGoalLocation);

        std::cout << "*LogEvents* - End - \t" << this->transfEventWGoalLocation->GetName()
                  << " [" << this->transfParticlePoolWGoalLocation.size() << "]"
                  << "\t\t at " << this->transfTsWGoalLocation << std::endl;

        // add grasp event to the map of all events
        this->nameToEvents_M[this->transfEventWGoalLocation->GetName()].push_back(this->transfEventWGoalLocation);

        // set pointer to NULL, do not delete it, since later we will access the memory using the saved events
        this->transfEventWGoalLocation = NULL;
        this->transfParticlePoolWGoalLocation.clear();
    }
}

//////////////////////////////////////////////////
void LogEvents::CheckTranslEventWFromLocation(
        const double _timestamp_ms,
        const bool _transf_detect_flag,
        const std::string _transf_from_model_name)
{
    // check if new transfer was detected
    // if transl ev not init, create event
    // TODO add init to events
    if(_transf_detect_flag && this->transfEventWFromLocation == NULL)
    {
        // TODO check for multiple particle types
        // save type of first particle from pool
        std::string particle_type = (*this->transfParticlePoolWFromLocation.begin())->GetParentModel()->GetName();

        // TODO property namespace, and porperty should belong to the OBJECT
        // create the contact GzEvent
        this->transfEventWFromLocation = new PpEvent(
                    "ParticleTranslation-" + particle_type + "-From-" + _transf_from_model_name,
                    "&knowrob_sim;", "ParticleTranslation",
                    "knowrob:", "particleType", _timestamp_ms);

        this->transfEventWFromLocation->AddObject(this->nameToEventObj_M[particle_type]);        

        this->transfEventWFromLocation->AddObject(new PpEventObj(_transf_from_model_name,
            "knowrob:","fromLocation"));
        
        std::cout << "*LogEvents* - Start - \t" << this->transfEventWFromLocation->GetName()
                  << " [" << particle_type << "]"
                  << "\t\t at " << _timestamp_ms << std::endl;
    }

    if((_timestamp_ms - this->transfTsWFromLocation > this->transfEvDurThresh) && this->transfEventWFromLocation != NULL)
    {
        // TODO hack for getting the nr of particles
        // add the nr of particles scooped
        //this->translEvent->AddObject(
        //               new GzEventObj(std::to_string(this->transfParticlePool.size())));

        // finish gz event
        // end grasping event
        this->transfEventWFromLocation->End(this->transfTsWFromLocation);

        std::cout << "*LogEvents* - End - \t" << this->transfEventWFromLocation->GetName()
                  << " [" << this->transfParticlePoolWFromLocation.size() << "]"
                  << "\t\t at " << this->transfTsWFromLocation << std::endl;

        // add grasp event to the map of all events
        this->nameToEvents_M[this->transfEventWFromLocation->GetName()].push_back(this->transfEventWFromLocation);

        // set pointer to NULL, do not delete it, since later we will access the memory using the saved events
        this->transfEventWFromLocation = NULL;
        this->transfParticlePoolWFromLocation.clear();
    }
}

//////////////////////////////////////////////////
void LogEvents::CheckModelTranslEvent(
        const double _timestamp_ms,
        const bool _model_transf_detect_flag)
{
    // check if new transfer was detected
    // if transl ev not init, create event
    // TODO add init to events
    if(_model_transf_detect_flag && this->modelTransfEvent == NULL)
    {
        // TODO check for multiple particle types
        // save type of first particle from pool
        const std::string model_type = this->transferedModel->GetName();

        // TODO property namespace, and porperty should belong to the OBJECT
        // create the contact GzEvent
        this->modelTransfEvent = new PpEvent(
                    "ObjectTransfer-" + model_type, "&knowrob_sim;", "ObjectTransfer",
                    "knowrob:", "objectType", _timestamp_ms);

        this->modelTransfEvent->AddObject(this->nameToEventObj_M[model_type]);
        //this->translEvent->AddObject(new GzEventObj(particle_type));

        std::cout << "*LogEvents* - Start - \t" << this->modelTransfEvent->GetName()
                  << "\t\t at " << _timestamp_ms << std::endl;
    }
    else if(!_model_transf_detect_flag && this->modelTransfEvent != NULL)
    {        
        // finish gz event
        // end grasping event
        this->modelTransfEvent->End(_timestamp_ms);

        std::cout << "*LogEvents* - End - \t" << this->modelTransfEvent->GetName()
                  << "\t\t at " << _timestamp_ms << std::endl;

        // add grasp event to the map of all events
        this->nameToEvents_M[this->modelTransfEvent->GetName()].push_back(this->modelTransfEvent);

        // set pointer to NULL, do not delete it, since later we will access the memory using the saved events
        this->modelTransfEvent = NULL;
    }
    
}

//////////////////////////////////////////////////
void LogEvents::CheckToolEvent(
        const double _timestamp_ms,
        const std::string _contact_with_tool_model,
        const std::string _tool_name,
        const std::string _tool_collision_name)
{
    // check for difference between current and prev grasp
    if (_contact_with_tool_model != this->prevContactWithToolModel)
    {
        if (!_contact_with_tool_model.empty())
        {
            this->prevContactWithToolModel = _contact_with_tool_model;

            // the name of the tool contact event
            std::string tool_contact_ev_name = "ToolContact-"
                    + _tool_name + + "[" + _tool_collision_name + "]-" 
                    + _contact_with_tool_model;

            // TODO why is if needed here? the two parts seem similar
            // check if the tool contact event has been initialized
            if(this->toolGzEvent != NULL)
            {
                // create the contact GzEvent
                this->toolGzEvent = new sg_pp::PpEvent(
                            tool_contact_ev_name, "&knowrob_sim;", "TouchingSituation",
                            "knowrob_sim:", "inContact", _timestamp_ms);

                // add the two objects in contact
                this->toolGzEvent->AddObject(this->nameToEventObj_M[_tool_name]);

                this->toolGzEvent->AddObject(this->nameToEventObj_M[_contact_with_tool_model]);
                
                //this->toolGzEvent->AddObject(new PpEventObj(_tool_collision_name,
                //"knowrob_sim:","toolPart"));
                this->toolGzEvent->AddObject(this->nameToEventObj_M[_tool_collision_name]);
                
                std::cout << "*LogEvents* - Start - \t" << tool_contact_ev_name
                          << "\t\t\t at " << _timestamp_ms  << std::endl;
            }
            // init first grasp
            else
            {
                // create the contact GzEvent
                this->toolGzEvent = new sg_pp::PpEvent(
                            tool_contact_ev_name, "&knowrob_sim;", "TouchingSituation",
                            "knowrob_sim:", "inContact", _timestamp_ms);

                // add the two objects in contact
                this->toolGzEvent->AddObject(this->nameToEventObj_M[_tool_name]);

                this->toolGzEvent->AddObject(this->nameToEventObj_M[_contact_with_tool_model]);
                
                //this->toolGzEvent->AddObject(new PpEventObj(_tool_collision_name,
                //"knowrob_sim:","toolPart"));
                this->toolGzEvent->AddObject(this->nameToEventObj_M[_tool_collision_name]);

                std::cout << "*LogEvents* - Init - \t" << tool_contact_ev_name
                          << "\t\t\t at " << _timestamp_ms  << std::endl;            }
        }
        else
        {
            // delete name
            this->prevContactWithToolModel.clear();

            // end grasping event
            this->toolGzEvent->End(_timestamp_ms);

            //TODO in order to remove the this->graspGzEvent != NULL check, we can delete this->graspevent and set it to NULL
            // similarly to transl events (or the other way around)

            std::cout << "*LogEvents* - End - \t" << this->toolGzEvent->GetName()
                      << "\t\t\t at " << _timestamp_ms  << std::endl;

            // add grasp event to the map of all events
            this->nameToEvents_M[this->toolGzEvent->GetName()].push_back(this->toolGzEvent);
        }
    }
}

//////////////////////////////////////////////////
void LogEvents::CheckToolPartEvent(
        const double _timestamp_ms,
        const std::string _contact_with_tool_part_model,
        const std::string _tool_part_name,
        const std::string _tool_part_collision_name)
{   
    // check for difference between current and prev
    if (_contact_with_tool_part_model != this->prevContactWithToolPartModel)
    {
        if (!_contact_with_tool_part_model.empty())
        {            
            this->prevContactWithToolPartModel = _contact_with_tool_part_model;

            // the name of the tool contact event
            std::string tool_contact_ev_name = "ToolContact-"
                    + _tool_part_name + "[" + _tool_part_collision_name + "]-" 
                    + _contact_with_tool_part_model;

            // TODO why is if needed here? the two parts seem similar
            // check if the tool contact event has been initialized
            if(this->toolPartGzEvent != NULL)
            {
                // create the contact GzEvent
                this->toolPartGzEvent = new sg_pp::PpEvent(
                            tool_contact_ev_name, "&knowrob_sim;", "TouchingSituation",
                            "knowrob_sim:", "inContact", _timestamp_ms);

                // add the two objects in contact
                this->toolPartGzEvent->AddObject(this->nameToEventObj_M[_tool_part_name]);

                this->toolPartGzEvent->AddObject(this->nameToEventObj_M[_contact_with_tool_part_model]);
                
                //this->toolPartGzEvent->AddObject(new PpEventObj(_tool_part_collision_name,
                //"knowrob_sim:","toolPart"));
                this->toolPartGzEvent->AddObject(this->nameToEventObj_M[_tool_part_collision_name]);

                std::cout << "*LogEvents* - Start - \t" << tool_contact_ev_name
                          << "\t\t\t at " << _timestamp_ms  << std::endl;
            }
            // init first
            else
            {
                // create the contact GzEvent
                this->toolPartGzEvent = new sg_pp::PpEvent(
                            tool_contact_ev_name, "&knowrob_sim;", "TouchingSituation",
                            "knowrob_sim:", "inContact", _timestamp_ms);

                // add the two objects in contact
                this->toolPartGzEvent->AddObject(this->nameToEventObj_M[_tool_part_name]);

                this->toolPartGzEvent->AddObject(this->nameToEventObj_M[_contact_with_tool_part_model]);
                
                //this->toolPartGzEvent->AddObject(new PpEventObj(_tool_part_collision_name,
                //"knowrob_sim:","toolPart"));
                this->toolPartGzEvent->AddObject(this->nameToEventObj_M[_tool_part_collision_name]);

                std::cout << "*LogEvents* - Init - \t" << tool_contact_ev_name
                          << "\t\t\t at " << _timestamp_ms  << std::endl;            }
        }
        else
        {
            // delete name
            this->prevContactWithToolPartModel.clear();

            // end grasping event
            this->toolPartGzEvent->End(_timestamp_ms);

            //TODO in order to remove the this->graspGzEvent != NULL check, we can delete this->graspevent and set it to NULL
            // similarly to transl events (or the other way around)

            std::cout << "*LogEvents* - End - \t" << this->toolPartGzEvent->GetName()
                      << "\t\t\t at " << _timestamp_ms  << std::endl;

            // add grasp event to the map of all events
            this->nameToEvents_M[this->toolPartGzEvent->GetName()].push_back(this->toolPartGzEvent);
        }
    }
}

//////////////////////////////////////////////////
void LogEvents::EndActiveEvents()
{
    // iterate through the map
    for(std::map<std::string, std::list<sg_pp::PpEvent*> >::const_iterator m_it = this->nameToEvents_M.begin();
            m_it != this->nameToEvents_M.end(); m_it++)
    {
        // iterate through the events with the same name
        for(std::list<PpEvent*>::const_iterator ev_it = m_it->second.begin();
                ev_it != m_it->second.end(); ev_it++)
        {
            // if event still open end it at the end time
            if((*ev_it)->IsOpen())
            {
                (*ev_it)->End(this->world->GetSimTime().Double());

                std::cout << "*LogEvents* - End - \t" << (*ev_it)->GetName() << "\t\t at "
                            << this->world->GetSimTime().Double() << std::endl;
            }
        }
    }
}

//////////////////////////////////////////////////
void LogEvents::RemoveGraspJitters()
{
    std::cout << "*LogEvents* - Removing small grasp events" << std::endl;
   
    // iterate through the map
    for(std::map<std::string, std::list<sg_pp::PpEvent*> >::iterator m_it = this->nameToEvents_M.begin();
            m_it != this->nameToEvents_M.end(); m_it++)
    {
        // iterate through the events with the same name
        for(std::list<PpEvent*>::iterator ev_it = m_it->second.begin();
                ev_it != m_it->second.end(); /*increment in the loop*/)
        {
            // if event of type grasp and smaller than the given value, remove it from the events
            if(((*ev_it)->GetClass() == "GraspingSomething") && ((*ev_it)->GetDuration() < this->graspJitterDur))
            {
                std::cout << "\t removing " <<  (*ev_it)->GetName()
                << "\t" << (*ev_it)->GetStartTime() << " <----> " << (*ev_it)->GetEndTime() << std::endl;
                            
                // remove current value from list
                ev_it = m_it->second.erase(ev_it);
            }
            else
            {
             ++ev_it;   
            }
        }
    }
}

//////////////////////////////////////////////////
void LogEvents::MergeEventDisconnections()
{
    std::cout << "*LogEvents* - Merging event disconnections" << std::endl;

    // iterate through the map
    for(std::map<std::string, std::list<sg_pp::PpEvent*> >::iterator m_it = this->nameToEvents_M.begin();
            m_it != this->nameToEvents_M.end(); m_it++)
    {
        // TODO use vector, with next?
        // iterate through the events with the same name
        for(std::list<PpEvent*>::iterator ev_it = m_it->second.begin();
                ev_it != m_it->second.end(); /*increment in the loop*/)
        {
            // save current event for comparison
            std::list<PpEvent*>::iterator curr_ev = ev_it;

            // increment current iteration (next event)
            ev_it++;

            // check that the next value is not the last
            if(ev_it != m_it->second.end())
            {
                // merge if the duration between the events is smaller than the given thresh
                if((*ev_it)->GetStartTime() - (*curr_ev)->GetEndTime() < this->eventDiscThresh)
                {
                    std::cout << "\t merging " <<  (*ev_it)->GetName()
                            << "\t" << (*curr_ev)->GetEndTime() << " --><-- " << (*ev_it)->GetStartTime() << std::endl;

                    // set the next values start time
                    (*ev_it)->SetStartTime((*curr_ev)->GetStartTime());

                    // remove current value from list
                    m_it->second.erase(curr_ev);
                }
            }
        }
    }
}

//////////////////////////////////////////////////
void LogEvents::WriteContexts()
{
    std::cout << "*LogEvents* - Writing contexts" << std::endl;

    // Write to owl file
    if(this->logLocation == "owl" || this->logLocation == "all")
    {            
        // initialize the beliefstate
        this->beliefStateClient = new beliefstate_client::BeliefstateClient("bs_client");

        // starting new experiment
        this->beliefStateClient->startNewExperiment();
        
        // adding experiment name to the meta data
        this->beliefStateClient->setMetaDataField("experiment", this->collName);

        // TODO issue with setmatadatafield
        // adding experiment meta data with all the tools from the experiment
        //for(const auto m_iter : this->world->GetModels())
        //{
        //  this->beliefStateClient->setMetaDataField("occuringObject", m_iter->GetName());
        //}

        // register the OWL namespace
        this->beliefStateClient->registerOWLNamespace("knowrob_sim",
                        "http://knowrob.org/kb/knowrob_sim.owl#");

        // iterate through the map
        for(std::map<std::string, std::list<sg_pp::PpEvent*> >::const_iterator m_it = this->nameToEvents_M.begin();
                m_it != this->nameToEvents_M.end(); m_it++)
        {            
            // iterate through the events with the same name
            for(std::list<sg_pp::PpEvent*>::const_iterator ev_it = m_it->second.begin();
                    ev_it != m_it->second.end(); ev_it++)
            {
                // create local belief state context
                beliefstate_client::Context* curr_ctx;

                // open belief state context
                curr_ctx = new beliefstate_client::Context(this->beliefStateClient,
                        (*ev_it)->GetName(), (*ev_it)->GetClassNamespace(),
                        (*ev_it)->GetClass(), (*ev_it)->GetStartTime() + (this->timeOffset * this->suffixTime));

                // get the objects of the event
                std::vector<PpEventObj*> curr_objects = (*ev_it)->GetObjects();

                // iterate through the objects
                for(std::vector<PpEventObj*>::const_iterator obj_it = curr_objects.begin();
                        obj_it != curr_objects.end(); obj_it++)
                {
                    // TODO check first if object in the map, if not create a new one
                    // (hack version because of adding numbers as objects)
                    if (this->nameToBsObject_M.find((*obj_it)->GetName()) != this->nameToBsObject_M.end()
                        && (*obj_it)->GetProperty() == "")
                    {
                        // TODO, this is needed to ensure unique hash of the same objects
                        // add object to the context
                        curr_ctx->addObject(
                                    this->nameToBsObject_M[(*obj_it)->GetName()],
                                (*ev_it)->GetPropertyNamespace() + (*ev_it)->GetProperty());
                    }
                    else if ((*obj_it)->GetProperty() == "")// create new object
                    {
                        // TODO
                        // we hackingly add numbers as transl particles
                        curr_ctx->addObject(
                                    new beliefstate_client::Object("&knowrob;", (*obj_it)->GetName()),
                                    (*ev_it)->GetPropertyNamespace() + (*ev_it)->GetProperty());
                    }
                    else if ((*obj_it)->GetProperty() != "")
                    {
                        // if obj already saved
                        if (this->nameToBsObject_M.find((*obj_it)->GetName()) != this->nameToBsObject_M.end())
                        {
                            curr_ctx->addObject(
                                this->nameToBsObject_M[(*obj_it)->GetName()],
                                (*obj_it)->GetPropertyNamespace() + (*obj_it)->GetProperty()); 
                        }
                        // else create it
                        else
                        {
                            curr_ctx->addObject(
                                    new beliefstate_client::Object("&knowrob;", (*obj_it)->GetName()),
                                    (*obj_it)->GetPropertyNamespace() + (*obj_it)->GetProperty());                        
                        }
                    }
                }
                
                // check if acat adt properties should be added to the current events
                if(this->acatADT)
                {                    
                    // add adt action roles
                    LogEvents::WriteADTActions(*ev_it, curr_ctx);
                }

                // end belief state context
                curr_ctx->end(true, (*ev_it)->GetEndTime() + (this->timeOffset * this->suffixTime));
            }
        }
        
        // check if acat adt properties should be added, register namespace, add prac data
        if(this->acatADT)
        { 
            // register the acat OWL namespace
            this->beliefStateClient->registerOWLNamespace("acat",
                "http://knowrob.org/kb/acat.owl#");
            
            std::string instruction;
            // check for instruction
            for(auto adt_obj : this->pracAdtObj)
            {
                if (std::get<0>(adt_obj) == "instruction")
                {
                    instruction = std::get<1>(adt_obj);
                }            
            }
            
            // open belief state context
            beliefstate_client::Context* prac_adt_ctx = new beliefstate_client::Context(
                this->beliefStateClient, instruction, "&acat;", "PracAdt", 0);
            
            // add objects
            for(auto adt_obj : this->pracAdtObj)
            {
                prac_adt_ctx->addObject(new beliefstate_client::Object("",std::get<1>(adt_obj)),
                                        "acat:" + this->ToCamelCaseWithoutFirst(std::get<0>(adt_obj)));
            }
            
            // end ctx 
            prac_adt_ctx->end(true, 0);            
        }
        
        // export belief state client
        this->beliefStateClient->exportFiles(this->collName);
    }

    // Write to mongodb
    if(this->logLocation == "mongo" || this->logLocation == "all")
    {
        // insert document object into the database, use scoped connection
        ScopedDbConnection scoped_connection("localhost");

        // iterate through the map
        for(std::map<std::string, std::list<sg_pp::PpEvent*> >::const_iterator m_it = this->nameToEvents_M.begin();
                m_it != this->nameToEvents_M.end(); m_it++)
        {
            // TODO fix the grasp issue, (grasp event map includes the actual events with the right names)
            // iterate through the events with the same name
            for(std::list<PpEvent*>::const_iterator ev_it = m_it->second.begin();
                    ev_it != m_it->second.end(); ev_it++)
            {
                // all events
                BSONObjBuilder event_bb;

                // add to the time array
                event_bb.append("event", (*ev_it)->GetName())
                        .append("start",(*ev_it)->GetStartTime())
                        .append("end", (*ev_it)->GetEndTime());

                scoped_connection->insert(this->dbName + "." + this->collName + "_ev",
                        event_bb.obj());
            }
        }

        // let the pool know the connection is done
        scoped_connection.done();
    }
}

//////////////////////////////////////////////////
void LogEvents::WriteTimelines()
{
    std::cout << "*LogEvents* - Writing timelines" << std::endl;

    // create file
    std::ofstream timeline_file;

    // create directory
    boost::filesystem::create_directory("timelines");

    // path stringstream
    std::stringstream path_ss;

    path_ss << "timelines/tl_" << this->collName << ".html";

    // open and add values to the file
    timeline_file.open(path_ss.str().c_str());

    timeline_file <<
            "<html>\n"
            "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization',\n"
            "       'version':'1','packages':['timeline']}]}\"></script>\n"
            "<script type=\"text/javascript\">\n"
            "google.setOnLoadCallback(drawChart);\n"
            "\n"
            "function drawChart() {\n"
            "  var container = document.getElementById('sim_timeline_ex');\n"
            "\n"
            "  var chart = new google.visualization.Timeline(container);\n"
            "\n"
            "  var dataTable = new google.visualization.DataTable();\n"
            "\n"
            "  dataTable.addColumn({ type: 'string', id: 'Event' });\n"
            "  dataTable.addColumn({ type: 'number', id: 'Start' });\n"
            "  dataTable.addColumn({ type: 'number', id: 'End' });\n"
            "\n"
            "  dataTable.addRows([\n";

    // iterate through the map
    for(std::map<std::string, std::list<sg_pp::PpEvent*> >::const_iterator m_it = this->nameToEvents_M.begin();
            m_it != this->nameToEvents_M.end(); m_it++)
    {
        // iterate through the events with the same name
        for(std::list<PpEvent*>::const_iterator ev_it = m_it->second.begin();
                ev_it != m_it->second.end(); ev_it++)
        {
            // add all events to the timeline file
            timeline_file << "    [ '" << (*ev_it)->GetName() << "', "
                    << (*ev_it)->GetStartTime() * 1000 << ", "
                    << (*ev_it)->GetEndTime() * 1000 << "],\n";
        }
    }

    timeline_file <<
            "  ]);\n"
            "\n"
            "  chart.draw(dataTable);\n"
            "}\n"
            "</script>\n"
            "<div id=\"sim_timeline_ex\" style=\"width: 1300px; height: 900px;\"></div>\n"
            "\n"
            "</html>";

    // close file
    timeline_file.close();
}

//////////////////////////////////////////////////
void LogEvents::WriteADTActions(PpEvent* _pp_event, beliefstate_client::Context* _ctx)
{
    if(_pp_event->GetClass() == "TurningOnPoweredDevice")
    {
        // get device acted on
        PpEventObj* obj = _pp_event->GetObjects().front();
        _ctx->addObject(this->nameToBsObject_M[obj->GetName()],"acat:device");
    }
    else if(_pp_event->GetClass() == "TurningOffPoweredDevice")
    {
        // get device acted on
        PpEventObj* obj = _pp_event->GetObjects().front();
        _ctx->addObject(this->nameToBsObject_M[obj->GetName()],"acat:device");
    }
    else if(_pp_event->GetClass() == "Pipetting")
    {
        // get device acted on
        PpEventObj* obj = _pp_event->GetObjects().front();
        
        _ctx->addObject(this->nameToBsObject_M[obj->GetName()],"acat:content");
        
        _ctx->addObject(this->nameToBsObject_M["ErlenmeyerFlask"],"acat:goal");
        
        _ctx->addObject(this->nameToBsObject_M["MeasuringCylinder"],"acat:from");
        
        _ctx->addObject(this->nameToBsObject_M["Pipette"],"acat:tool");
    } 
    else if(_pp_event->GetClass() == "ParticleTranslation")
    {
        // get device acted on
        PpEventObj* obj = _pp_event->GetObjects().front();
        
        _ctx->addObject(this->nameToBsObject_M[obj->GetName()],"acat:stuff");
    }
    else if(_pp_event->GetClass() == "TouchingSituation")
    { 
        bool is_supporting_event = true;
        
        // iterate through the event objects to check that is a supporting situation
        for(auto obj : _pp_event->GetObjects())
        {
            if (obj->GetProperty() == "toolPart")
            {
                is_supporting_event = false;
            }
        }
        
        if (is_supporting_event)
        {        
            math::Vector3 supporting_plane_pos;
            
            // flag to check if obj1 is of type surface
            bool obj1_surface_flag = false;

            // get the objects colliding
            PpEventObj* obj1 = _pp_event->GetObjects().front();
            PpEventObj* obj2 = _pp_event->GetObjects().back();

            // check which object has the surface event collision
            // loop through the links
            for (const auto l_iter : this->world->GetModel(obj1->GetName())->GetLinks())
            {
                // loop through all the collision
                for (const auto c_iter : l_iter->GetCollisions())
                {
                    // check if a collide without contact collision is available
                    if (c_iter->GetSurface()->collideWithoutContact)
                    {
                        // obj1 is of type surface
                        obj1_surface_flag = true;
                        
                        // add Z axis of the obj1 collision to the supporting plane
                        supporting_plane_pos.z = c_iter->GetWorldPose().pos.z;
                        
                        // add X Y from obj2
                        supporting_plane_pos.x = this->world->GetModel(obj2->GetName())->GetWorldPose().pos.x;            
                        supporting_plane_pos.y = this->world->GetModel(obj2->GetName())->GetWorldPose().pos.y;            
                    }
                }
            }

            // if obj1 is not of type surface, check for obj2
            if (!obj1_surface_flag)
            {
                // check which object has the surface event collision
                // loop through the links
                for (const auto l_iter : this->world->GetModel(obj2->GetName())->GetLinks())
                {
                    // loop through all the collision
                    for (const auto c_iter : l_iter->GetCollisions())
                    {
                        // check if a collide without contact collision is available
                        if (c_iter->GetSurface()->collideWithoutContact)
                        {  
                            // add Z axis of the obj1 collision to the supporting plane
                            supporting_plane_pos.z = c_iter->GetWorldPose().pos.z;
                            
                            // add X Y from obj2
                            supporting_plane_pos.x = this->world->GetModel(obj1->GetName())->GetWorldPose().pos.x;            
                            supporting_plane_pos.y = this->world->GetModel(obj1->GetName())->GetWorldPose().pos.y;            
                        }
                    }
                }            
            }

            // supporting plane as string
            const std::string str_pos = std::to_string(supporting_plane_pos.x) + "_"
                + std::to_string(supporting_plane_pos.y) + "_" 
                + std::to_string(supporting_plane_pos.z);
            
            _ctx->addObject(new beliefstate_client::Object("",str_pos),"acat:supportingPlane");

            // if obj1 is of type supporting 
            if (obj1_surface_flag)
            {           
                _ctx->addObject(this->nameToBsObject_M[obj2->GetName()],"acat:supportedObject");  
            }
            else
            {
                _ctx->addObject(this->nameToBsObject_M[obj2->GetName()],"acat:supportedObject");  

            }
        }
    }
}

/////////////////////////////////////////////////
std::string LogEvents::ToCamelCase(const std::string _name)
{
    // change name to stringstream
    std::stringstream full_name_ss(_name);
        
    // return
    std::string camel_case;
    
    // vectors after tokenization
    std::vector<std::string> dot_items;
    std::vector<std::string> underscore_items;    
    std::string curr_item;
    
    // tokenize with dots
    while(std::getline(full_name_ss, curr_item, '.')) 
    {
        dot_items.push_back(curr_item);
    }
    
    // change name to stringstream
    std::stringstream underscore_name_ss(dot_items.at(0));
    
    // tokenize with underscore
    while(std::getline(underscore_name_ss, curr_item, '_'))
    {
        // uppercase the item and add it to the vector
        curr_item[0] = std::toupper(curr_item[0]);
        underscore_items.push_back(curr_item);
    }
    
    // append the items
    for(auto s : underscore_items)
    {
        camel_case+=s;
    }
    
    return camel_case;
}

/////////////////////////////////////////////////
std::string LogEvents::ToCamelCaseWithoutFirst(const std::string _name)
{
    // change name to stringstream
    std::stringstream full_name_ss(_name);
        
    // return
    std::string camel_case;
    
    // vectors after tokenization
    std::vector<std::string> dot_items;
    std::vector<std::string> underscore_items;    
    std::string curr_item;
    
    // tokenize with dots
    while(std::getline(full_name_ss, curr_item, '.')) 
    {
        dot_items.push_back(curr_item);
    }
    
    // change name to stringstream
    std::stringstream underscore_name_ss(dot_items.at(0));
    
    int i = 0;
    
    // tokenize with underscore
    while(std::getline(underscore_name_ss, curr_item, '_'))
    {
        // skip first item
        if (i!=0)
        {
            // uppercase the item and add it to the vector
            curr_item[0] = std::toupper(curr_item[0]);
        }
        underscore_items.push_back(curr_item);
        i++;
    }
    
    
    // append the items
    for(auto s : underscore_items)
    {
        camel_case+=s;
    }
    
    return camel_case;
}